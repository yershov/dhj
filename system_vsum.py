import math
import numpy as np
import itertools
from system import SystemError

tol = 1.e-30
tol1 = 1.e-7

def minloc(X, G, V, w, pf=False):
    n = len(V)
    if pf:
        print "n=", n
    m = len(G)
    if pf:
        print "m=", m
    X = np.array(X, dtype=np.float32).T
    if pf:
        print "X=", X
    G = np.array(G, dtype=np.float32).T
    if pf:
        print "G=", G
    V = np.array(V, dtype=np.float32)
    if pf:
        print "V=", V
    I = np.ones(n, dtype=np.float32)
    if pf:
        print "I=", I

    Q = np.dot(np.linalg.pinv(X), G)
    if pf:
        print "Q=", Q
    
    i = np.dot(I, Q)
    if pf:
        print "i=", i
    v = np.dot(V, Q)
    if pf:
        print "v=", v

    c = np.dot(Q, v)
    if pf:
        print "c=", c
    d = np.dot(Q, i)
    if pf:
        print "d=", d
    s = filter(lambda ind: c[ind] > 0.0, xrange(n))
    if pf:
        print "s=", s
    
    V_cross = np.divide(c, d + tol * I)
    V_order = np.argsort(V_cross)
    if pf:
        print "V*=", V_cross
        print "i*=",V_order
    V_order = V_order[V_cross[V_order] > 0.0]
    V_cross = V_cross[V_order]
    if pf:
        print "V*=", V_cross
        print "i*=",V_order

    l = V_order.size
    if pf:
        print "l=", l

    E = np.eye(m, dtype=np.float32)
#    print "E=", E

    remove = False

    for k in xrange(l+1):
        if k > 0:
            ind = V_order[k-1]
            remove = (ind in s)
            if remove:
                s.remove(ind)
            else:
                s.append(ind)

        if remove:
            a = np.vstack((a,np.dot(P, v - V_cross[k-1] * i)))                

        if s:
            Qs = Q[s]
#            print "Qs=", Qs
            P = E - np.dot(np.linalg.pinv(Qs), Qs)
        else:
#            print "Qs=", 0
            P = E
#        print "P=", P
        if k == 0:
            a = np.dot(P, v)
            b = np.dot(P, i)
        else:
            if not remove:
                a = np.vstack((a,np.dot(P, v - V_cross[k-1] * i)))                
            b = np.vstack((b,np.dot(P, i)))

    if pf:
        print "a=", a
        print "b=", b

        
    V_cross = np.hstack((np.zeros(1), V_cross))
    if pf:
        print "V*=", V_cross

    if pf:
        for k in xrange(l):
            print a[k] - (V_cross[k+1] - V_cross[k]) * b[k]
            print a[k+1]
        
    
    eps_min = 0.0
    V_sol = 0.0
    first_eps = True

    V_star = 0.0

    for k in xrange(l+1):
        if k < l:
            dV_cross = V_cross[k+1] - V_cross[k]
            if pf:
                print "dV_cross=", dV_cross
        else:
            if pf:
                print "dV_cross= inf"            
        if l == 0:
            ak = a
            bk = b
        else:
            ak = a[k]
            bk = b[k]
        x = np.dot(bk,bk)
        y = np.dot(ak,bk)
        z = np.dot(ak,ak) - w2
        if pf:
            print "solving", x, "v**2-", 2*y,"v+",z,"=0" 
        D = y**2 - x*z
        if pf:
            print "D=", D
        if abs(x) < tol1:
            eps = abs(z)
            if first_eps or eps < eps_min:
                first_eps = False
                eps_min = eps
                V_star = V_cross[k]
        elif D < 0.0:
            dV_star = y/x
            if pf:
                print "dV_star=", dV_star
            if dV_star < 0.0:
                dV_star = 0.0
            elif k < l and dV_star > dV_cross:
                dV_star = dV_cross
            eps = abs(x*dV_star**2 - 2.0*y*dV_star + z)
            if first_eps or eps < eps_min:
                first_eps = False
                eps_min = eps
                V_star = V_cross[k] + dV_star
        else:
            dV_star1 = (y - math.sqrt(D)) / x
            dV_star2 = (y + math.sqrt(D)) / x
            if pf:
                print "dV_star1=", dV_star1
                print "dV_star2=", dV_star2
            if dV_star1 >= 0.0 and (k == l or dV_star1 < dV_cross):
                V_star = V_cross[k] + dV_star1
                eps_min = 0.0
                break
            elif dV_star2 >= 0.0 and (k == l or dV_star2 < dV_cross):
                V_star = V_cross[k] + dV_star2
                eps_min = 0.0
                break
            elif dV_star2 < 0.0:
                if first_eps or z < eps_min:
                    eps_min = z
                    V_star = V_cross[k]
            elif dV_star1 > dV_cross:
                eps = x*dV_cross**2 - 2.0*y*dV_cross + z
                if first_eps or eps < eps_min:
                    eps_min = eps
                    V_star = V_cross[k+1]
            else:
                eps = -(x*dV_cross**2 - 2.0*y*dV_cross)
                if eps > 0:
                    eps = -z
                    if first_eps or eps < eps_min:
                        eps_min = eps
                        V_star = V_cross[k]
                else:
                    eps -= z
                    if first_eps or eps < eps_min:
                        eps_min = eps
                        V_star = V_cross[k+1]
        if pf:
            print "eps_min=", eps_min
            print "V_star=", V_star
    return V_star, eps_min

class OmniVehicle(object):

    cartesian_number = 0
    topological_dimension = []
    weight_function = None
    speed_function = None
    weight_node = 'vertex'

    def __init__(self, mesh, speed_function = (lambda x: 1.0), weight_function = (lambda x: 1.0), weight_node = 'vertex', short_stencil=False):
        self.cartesian_number = mesh.cartesian_number()
        self.topological_dimension = mesh.topological_dimensions()
        self.weight_function = weight_function
        self.speed_function = speed_function
        self.weight_node = weight_node
        self.short_stencil = short_stencil

    def compute_id(self, node_id):
        result = node_id[-1]
        for d in xrange(1, self.cartesian_number):
            result = node_id[-1-d] + (self.topological_dimension[d] + 1) * result
        return result


    def update(self, nodes, V):
        weight = self.weight(nodes)
        
        if weight == float('inf'):
            return float('inf'), [], []
#        if nodes[0][0] > 0.45 and nodes[0][0] < 0.55 and nodes[0][1] > 0.45 and nodes[0][1] < 0.55:
#            print nodes[0], weight

        if self.cartesian_number == 1:
            return minloc(nodes, V, weight)

        if self.short_stencil:
            node_id = [0] * self.cartesian_number
            index = self.compute_id(node_id)
            virtual_nodes = [nodes[index]]
            virtual_V = [V[index]]
            for i in xrange(self.cartesian_number):
                for j in xrange(self.topological_dimension[i]):
                    node_id[i] = j+1
                    index = self.compute_id(node_id)
                    virtual_nodes.append(nodes[index])
                    virtual_V.append(V[index])
                node_id[i] = 0
            return minloc(virtual_nodes, virtual_V, weight)

        permutations = [[i]*self.topological_dimension[i] for i in xrange(self.cartesian_number)]
        permutations = sum(permutations, [])
        permutations = set(itertools.permutations(permutations))

        V_min = float('inf')
        node_min = []
        beta_min = []

        for permutation in permutations:
            node_id = [0] * self.cartesian_number
            index = self.compute_id(node_id)
            virtual_nodes = [nodes[index]]
            virtual_V = [V[index]]
            for p in permutation:
                node_id[p] += 1
                index = self.compute_id(node_id)
                virtual_nodes.append(nodes[index])
                virtual_V.append(V[index])

            V_loc, j_dep, beta = minloc(virtual_nodes, virtual_V, weight)

            if V_loc < V_min:
                V_min = V_loc
                node_id = [0] * self.cartesian_number
                node_min = [[0]*self.cartesian_number]
                for p in permutation:
                    node_id[p] += 1
                    node_min.append(self.compute_id(node_id))
                node_min = [node_min[j] for j in j_dep]
                beta_min = beta

        return V_min, node_min, beta_min


    def weight(self, nodes, pf = False):
        node_number = len(nodes)
        geometric_dimension = len(nodes[0])
        test_node = [0.0] * geometric_dimension
        if self.weight_node == 'barycenter':
            for node in nodes:
                test_node = map(lambda a, b: a+b, node, test_node)
            alpha = 1.0 / node_number
            for i in xrange(geometric_dimension):
                test_node[i] = test_node[i] * alpha
        if self.weight_node == 'vertex':
            test_node = nodes[0]

        speed = self.speed_function(test_node)
        if speed == 0.0:
            return float('inf')
        weight = self.weight_function(test_node)
        return weight / speed
        


# class OmniWeighted(OmniVehicle):

#     def __init__(self, mesh, weight_function = (lambda x: 1.0), weight_node = 'baricenter'):
#         OmniVehicle.__init__(self, mesh)
#         self.weight_function = weight_function
#         self.weight_node = weight_node


#     def update(self, nodes, V):
# # compute the weight from weight_function
#         print nodes
#         weight = self.weight(nodes)
#         print weight
#         return OmniWeighted.update(nodes, V, weight)

    def weightspeed_field(self, mesh, results):
        value_index = len(results.node_values_names)
        results.node_values_mask.append(value_index)
        results.node_values_names.append('weight')
        
        for i in xrange(len(results.node_values)):
            node = results.node_values[i]
            node[1].append(self.weight([mesh.get_coordinates(node[0]),]))


