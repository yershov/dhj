import math
import itertools
from utils import *
from system import SystemError

def minloc(x, V, c):
    topological_dimension = len(x) - 1

    j_dep = filter(lambda j : not (math.isinf(V[j]) or math.isnan(V[j])),  xrange(1, topological_dimension+1))
    
    while True:

        if j_dep == []:
            return float('inf'), [], []

        n_j_dep = len(j_dep)

        if n_j_dep == 1:
            j = j_dep[0]
            V0 = V[j] + c * norm(vector_diff(x[j], x[0]))
            return V0, j_dep, [1.0]

##
##      compute X = [x1 - x0, x2 - x0, ..., xn - x0]^T
##
        X = [vector_diff(x[j], x[0]) for j in j_dep]

##
##      compute P = X^T X
##
#        print "X=", X

        P = [[dot(X[j_id1], X[j_id2]) if j_id2 >= j_id1 else 0.0 for j_id2 in xrange(n_j_dep)] for j_id1 in xrange(n_j_dep)]
        for j_id1 in xrange(n_j_dep):
            for j_id2 in xrange(j_id1):
                P[j_id1][j_id2] = P[j_id2][j_id1]

#        print "P=", P

##
##      compute Q = P^{-1}
##

        Q = np.linalg.inv(P).tolist()

#        print "Q=", Q

        DV = [V[j] for j in j_dep]

#        print "DV=", DV
##
##      compute QIsum = [1, 1, ..., 1] * Q
##
##      and QVsum = [V1, V2, ..., Vn] * Q
##            

        QIsum = [sum(Q[j_id]) for j_id in xrange(n_j_dep)]
        QVsum = [dot(DV, Q[j_id]) for j_id in xrange(n_j_dep)]

#        print "QIsum=", QIsum
#        print "QVsum=", QVsum
##
##      solve V0^2 I^T Q I - 2 V0 DV^T Q I + DV^T Q DV - c^2 = 0
##

        aiai = sum(QIsum)
        aiaj = sum(QVsum)
        ajaj = dot(QVsum, DV)

#        print "aiai=", aiai
#        print "aiaj=", aiaj
#        print "ajaj=", ajaj

        D = (aiaj**2 - aiai * (ajaj - c**2))
            
#        print "D=", D

        if D < 0.0:
            j_max = j_dep[0]
            for j in j_dep[1:]:
                if V[j] > V[j_max]:
                    j_max = j
            j_dep.remove(j_max)
            continue
            
        V0 = (aiaj + math.sqrt(D)) / aiai

##
##      Verify the constraintes < grad_V, alphaj > <= 0 for all j = 1, ..., n
##      Remove vertices for which constraints are not satisfied
##
        beta = vector_diff(QVsum, vector_scale(V0, QIsum))
        beta_dep = [b > 0.0 for b in beta]
        if any(beta_dep):
            j_dep = [j_dep[j_id] for j_id in xrange(n_j_dep) if not beta_dep[j_id]]
            continue

        nml = sum(beta)
        beta = vector_scale(1.0 / nml, beta)
        break

    return V0, j_dep, beta


class OmniVehicle(object):

    cartesian_number = 0
    topological_dimension = []
    weight_function = None
    speed_function = None
    weight_node = 'vertex'

    def __init__(self, mesh, speed_function = (lambda x: 1.0), weight_function = (lambda x: 1.0), weight_node = 'vertex', short_stencil=False):
        self.cartesian_number = mesh.cartesian_number()
        self.topological_dimension = mesh.topological_dimensions()
        self.weight_function = weight_function
        self.speed_function = speed_function
        self.weight_node = weight_node
        self.short_stencil = short_stencil

    def compute_id(self, node_id):
        result = node_id[-1]
        for d in xrange(1, self.cartesian_number):
            result = node_id[-1-d] + (self.topological_dimension[d] + 1) * result
        return result


    def update(self, nodes, V):
        weight = self.weight(nodes)
        
        if weight == float('inf'):
            return float('inf'), [], []
#        if nodes[0][0] > 0.45 and nodes[0][0] < 0.55 and nodes[0][1] > 0.45 and nodes[0][1] < 0.55:
#            print nodes[0], weight

        if self.cartesian_number == 1:
            return minloc(nodes, V, weight)

        if self.short_stencil:
            node_id = [0] * self.cartesian_number
            index = self.compute_id(node_id)
            virtual_nodes = [nodes[index]]
            virtual_V = [V[index]]
            for i in xrange(self.cartesian_number):
                for j in xrange(self.topological_dimension[i]):
                    node_id[i] = j+1
                    index = self.compute_id(node_id)
                    virtual_nodes.append(nodes[index])
                    virtual_V.append(V[index])
                node_id[i] = 0
            return minloc(virtual_nodes, virtual_V, weight)

        permutations = [[i]*self.topological_dimension[i] for i in xrange(self.cartesian_number)]
        permutations = sum(permutations, [])
        permutations = set(itertools.permutations(permutations))

        V_min = float('inf')
        node_min = []
        beta_min = []

        for permutation in permutations:
            node_id = [0] * self.cartesian_number
            index = self.compute_id(node_id)
            virtual_nodes = [nodes[index]]
            virtual_V = [V[index]]
            for p in permutation:
                node_id[p] += 1
                index = self.compute_id(node_id)
                virtual_nodes.append(nodes[index])
                virtual_V.append(V[index])

            V_loc, j_dep, beta = minloc(virtual_nodes, virtual_V, weight)

            if V_loc < V_min:
                V_min = V_loc
                node_id = [0] * self.cartesian_number
                node_min = [[0]*self.cartesian_number]
                for p in permutation:
                    node_id[p] += 1
                    node_min.append(self.compute_id(node_id))
                node_min = [node_min[j] for j in j_dep]
                beta_min = beta

        return V_min, node_min, beta_min


    def weight(self, nodes, pf = False):
        node_number = len(nodes)
        geometric_dimension = len(nodes[0])
        test_node = [0.0] * geometric_dimension
        if self.weight_node == 'barycenter':
            for node in nodes:
                test_node = map(lambda a, b: a+b, node, test_node)
            alpha = 1.0 / node_number
            for i in xrange(geometric_dimension):
                test_node[i] = test_node[i] * alpha
        if self.weight_node == 'vertex':
            test_node = nodes[0]

        speed = self.speed_function(test_node)
        if speed == 0.0:
            return float('inf')
        weight = self.weight_function(test_node)
        return weight / speed
        


# class OmniWeighted(OmniVehicle):

#     def __init__(self, mesh, weight_function = (lambda x: 1.0), weight_node = 'baricenter'):
#         OmniVehicle.__init__(self, mesh)
#         self.weight_function = weight_function
#         self.weight_node = weight_node


#     def update(self, nodes, V):
# # compute the weight from weight_function
#         print nodes
#         weight = self.weight(nodes)
#         print weight
#         return OmniWeighted.update(nodes, V, weight)

    def weightspeed_field(self, mesh, results):
        value_index = len(results.node_values_names)
        results.node_values_mask.append(value_index)
        results.node_values_names.append('weight')
        
        for i in xrange(len(results.node_values)):
            node = results.node_values[i]
            node[1].append(self.weight([mesh.get_coordinates(node[0]),]))


