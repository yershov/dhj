from utils import *
from itertools import *
import time

class MarkerError(Exception):
    def __init__(self, value=""):
        self.value = value
    def __str__(self):
        return repr(self.value)

class IncrementalMarker:
    def __init__(self, marker):
        self.marker = marker
        self.__marked_simplices = SortedList(self.marker.mark_simplices())
        self.__marked_nodes = SortedList(self.marker.mark_nodes())

    def add_simplices(self, simplex_ids):
        for simplex_id in simplex_ids:
            if self.marker.test_simplex(simplex_id):
                self.__marked_simplices.insert(simplex_id)

    def add_nodes(self, node_ids):
        for node_id in self.__marked_nodes:
            if not self.marker.test_node(node_id):
                self.__marked_nodes.remove(node_id)
        for node_id in node_ids:
            if self.marker.test_node(node_id):
                self.__marked_nodes.insert(node_id)

    def remove_simplices(self, simplex_ids):
        for simplex_id in simplex_ids:
            try:
                self.__marked_simplices.remove(simplex_id)
            except ValueError:
                pass

    def remove_nodes(self, node_ids):
        for node_id in node_ids:
            try:
                self.__marked_nodes.remove(node_id)
            except ValueError:
                pass
        
    def update_simplices(self, removed_simplex_ids, added_simplex_ids):
        self.remove_simplices(removed_simplex_ids)
        self.add_simplices(added_simplex_ids)

    def update_nodes(self, removed_node_ids, added_node_ids):
        self.remove_nodes(removed_node_ids)
        self.add_nodes(added_node_ids)

    def mark_simplices(self):
        return iter(self.__marked_simplices)

    def mark_nodes(self):
        return iter(self.__marked_nodes)


    def test_simplex(self, simplex_id):
        try:
            self.__marked_simplices.search(simplex_id)
        except ValueError:
            return False
        else:
            return True

    def test_node(self, node_id):
        try:
            self.__marked_nodes.search(node_id)
        except ValueError:
            return False
        else:
            return True

class AbstractMarker:
    def __init__(self, mesh):
        self.mesh = mesh
    
    def reinitialize(self):
        return

    def test_simplex(self, simplex_id):
        raise NotImplementedError('test_simplex is not implemented')

    def test_node(self, node_id):
        raise NotImplementedError('test_node is not implemented')

    def mark_simplices(self):
        return ifilter(self.test_simplex, self.mesh.simplex_iterator())

    def mark_nodes(self):
        return ifilter(self.test_node, self.mesh.node_iterator())


class AndMarker(AbstractMarker):
    def __init__(self, mesh, *markers):
        AbstractMarker.__init__(self, mesh)
        self.markers = markers

    def reinitialize(self):
        for marker in self.markers:
            marker.reinitialize()

#  todo: think about logic carefully here

    def test_simplex(self, simplex_id):
        for marker in self.markers:
            if not marker.test_simplex(simplex_id):
                return False
        return True

    def test_node(self, node_id):
        for marker in self.markers:
            if not marker.test_node(node_id):
                return False
        return True

class OrMarker(AbstractMarker):
    def __init__(self, mesh, *markers):
        AbstractMarker.__init__(self, mesh)
        self.markers = markers

    def reinitialize(self):
        for marker in self.markers:
            marker.reinitialize()

    def test_simplex(self, simplex_id):
        for marker in self.markers:
            if marker.test_simplex(simplex_id):
                return True
        return False

    def test_node(self, node_id):
        for marker in self.markers:
            if marker.test_node(node_id):
                return True
        return False


class NotMarker(AbstractMarker):
    def __init__(self, mesh, marker):
        AbstractMarker.__init__(self, mesh)
        self.marker = marker

    def reinitialize(self):
        self.marker.reinitialize()

    def test_simplex(self, simplex_id):
        return not self.marker.test_simplex(simplex_id)

    def test_node(self, node_id):
        return not self.marker.test_node(node_id)
    

class AllMarker(AbstractMarker):

    def __init__(self, mesh):
        AbstractMarker.__init__(self, mesh)

    def test_simplex(self, simplex_id):
        return True

    def test_node(self, node_id):
        return True

class QualityMarker(AbstractMarker):

    def __init__(self, mesh, qp, qt):
        AbstractMarker.__init__(self, mesh)
        self.qp = qp
        self.qt = qt

    def test_simplex(self, simplex_id):
        return self.mesh.get_simplex_quality(simplex_id)[self.qp] < self.qt

    def test_node(self, node_id):
        for simplex_id in self.mesh.star(node_id):
            if self.test_simplex(simplex_id):
                return True
        return False

#
# todo: figure this one out
class CharacteristicsMarker(AbstractMarker):

    def __init__(self, mesh, results, x, badness=1.0):
        AbstractMarker.__init__(self, mesh)

        self.results = results
        
        self.x = x
        self.badness = (1.0 - badness)
        self.simplex_id = None
        self.reinitialize()

    def reinitialize(self):
        self.simplex_refine = [SortedList(duplicate=False) for mesh_id in xrange(self.mesh.cartesian_number())]
        self.node_refine = [SortedList(duplicate=False) for mesh_id in xrange(self.mesh.cartesian_number())]

        if self.results.node_values_names == None:
            return

        self.i_index = self.results.node_values_names.index('char_nodes')
        self.w_index = self.results.node_values_names.index('char_weights')
        self.s_index = self.results.node_values_names.index('char_simplex')

        self.simplex_id = self.mesh.find_simplex(self.x, self.simplex_id)

        node_ids = list(self.mesh.node_ids(self.simplex_id))

        self.marked_simplices = SortedList([self.simplex_id])

        while node_ids:
            node_id = node_ids.pop(0)

            for mesh_id in xrange(self.mesh.cartesian_number()):
                self.node_refine[mesh_id].insert(node_id[mesh_id])

            try:
                values = self.results.getnode(node_id)
            except KeyError:
                continue

            node_ids_neighbors = values[self.i_index]
            neighbor_weights = values[self.w_index]
            simplex_id = values[self.s_index]

            if simplex_id == None:
                continue

            try:
                self.marked_simplices.search(simplex_id)
            except ValueError:
                self.marked_simplices.insert(simplex_id)
            else:
                continue
            
            for mesh_id in xrange(self.mesh.cartesian_number()):
                self.simplex_refine[mesh_id].insert(simplex_id[mesh_id])
                
            for index in xrange(len(node_ids_neighbors)):
                if neighbor_weights[index] > self.badness:
                    node_ids += [node_ids_neighbors[index]]
            

    def test_simplex(self, simplex_id):
        try:
            self.simplex_refine[mesh_id].search(simplex_id)
        except ValueError:
            return False
        return True


    def test_node(self, node_id):
        try:
            self.node_refine[mesh_id].search(node_id)
        except ValueError:
            return False
        return True


class PointMarker(AbstractMarker):

    def __init__(self, mesh, x, node_marked = 'closest'):
        AbstractMarker.__init__(self, mesh)

        self.x = x
        self.node_marked = node_marked
        self.simplex_id = None

    def reinitialize(self):
        return

    def test_simplex(self, simplex_id):
        return simplex_id == self.mesh.find_simplex(self.x, self.simplex_id)

    def test_node(self, node_id):
        t0 = time.clock()
        self.simplex_id = self.mesh.find_simplex(self.x, self.simplex_id)
        if self.node_marked == 'closest':
            closest_node_id = None
            min_distance = 0.0
            for test_node_id in self.mesh.node_ids(self.simplex_id):
                distance = norm(vector_diff(self.x, self.mesh.get_coordinates(test_node_id)))
                if closest_node_id == None or distance < min_distance:
                    min_distance = distance
                    closest_node_id = test_node_id
            return node_id == closest_node_id
        if self.node_marked == 'all':
            for test_node_id in self.mesh.node_ids(self.simplex_id):
                if test_node_id == node_id:
                    return True
            return False

class ConvexPolytopeMarker(AbstractMarker):
    
    def __init__(self, mesh, x, n):
        AbstractMarker.__init__(self, mesh)
        if len(x) != len(n):
            raise MarkerError("polytope is ill-defined")
        self.x0 = x
        self.n = n
        

    def test_simplex(self, simplex_id):
        for node_id in self.mesh.node_ids(simplex_id):
            if not self.test_node(node_id):
                return False
        return True


    def test_node(self, node_id):
        x = self.mesh.get_coordinates(node_id)
        for (x0, n) in zip(self.x0, self.n):
            d = dot(vector_diff(x, x0), n)
            if d < 0.0 :
                return False
        return True
