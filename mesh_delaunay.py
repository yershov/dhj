import copy
import numpy as np
from utils import *
from math import sqrt
from itertools import *
from mesh import MeshError
from vtk_writer import write_vtu


def bounding_regular_simplex(d,c,r):
    x0 = (1.0 - sqrt(1.0 + d)) / d
    bc = (1.0 + x0) / (d + 1)
    r0 = - ( bc - 1.0 + bc * (d-1)) / sqrt(d)
    x = [map(lambda ci: (x0 - bc) * r / r0 + ci, c)]
    for i in xrange(d):
        e = [0.0]*d; e[i] = 1.0
        xi = map(lambda xi, ci: (xi - bc) * r / r0 + ci, e, c)
        x += [xi]
    return x
    

class DelaunayMesh:
    
    def __init__(self, dimension, bounding_nodes, lifting_function):
        self.dimension = dimension
        if len(bounding_nodes) != dimension+1:
            raise MeshError('Number of bounding nodes must be exactly ' + str(dimension+1) + ', but ' + str(len(bounding_nodes)) + ' are given')

        self.__lf = lifting_function

        self.__nodes = []
        self.__lfmax = 0.0
        X = []
        A = []
        b = []
        for node in bounding_nodes:
            self.__nodes += [node]
            node_lf = lifting_function(node)
            if node_lf > self.__lfmax:
                self.__lfmax = node_lf

            X += [node + [1.0]]
            a = node + [node_lf, 1.0]
            A += [a]
            b += [0.0]

        A += [[0.0]*self.dimension + [self.__lfmax, 1.0]]
        b += [-1.0]
        n = solve(A, b)

        for node in bounding_nodes:
            node_lf = lifting_function(node)
            a = node + [node_lf, 1.0]
        

        self.__simplex_plane = [n]
 
        self.__simplices = [range(self.dimension+1)]

        self.__children = [[]]
        self.__adjacency = [range(-1,-(self.dimension+2),-1)]

        self.__neighbors = [0]*(self.dimension+1)

        self.__barycentric = [np.linalg.inv(X).T]

        self.__quality = [self.__compute_quality(0)]

        self.max_visible = 0
        self.max_ridges = 0
        self.max_peaks = 0

        self.__node_number = dimension + 1
        self.__simplex_number = 1

        self.added_node_ids = []
        self.added_simplex_ids = []
        self.removed_simplex_ids = []

##
##  compute simplex quality
##
    def __compute_quality(self, simplex_id):
        nodes = self.__simplices[simplex_id]
        x = [self.__nodes[node_id] for node_id in nodes]
        lmin = float('inf')
        lmax = 0.0
        cosmin = float('inf')
        nodemin = None
        nodemax = None
        for node_id1 in nodes:
            x1 = self.__nodes[node_id1]
            for node_id2 in nodes:
                if node_id1 == node_id2:
                    continue
                x2 = self.__nodes[node_id2]
                x12 = vector_diff(x1,x2)
                l = norm(x12)
                if l > lmax:
                    lmax = l
                    nodemax = (node_id1, node_id2)
                if l < lmin:
                    lmin = l
                    nodemin = (node_id1, node_id2)
                for node_id3 in nodes:
                    x3 = self.__nodes[node_id3]
                    if node_id3 == node_id1 or node_id3 == node_id2:
                        continue
                    x32 = vector_diff(x3,x2)
                    cos = dot(x12,x32) / l / norm(x32)
                    if cos < cosmin:
                        cosmin = cos
                    
        CM = [[0] + [1] * (self.dimension+1)]
        for i in xrange(self.dimension+1):
            CMrow = [1]
            for j in xrange(self.dimension+1):
                xij = vector_diff(x[i], x[j])
                lij = dot(xij, xij)
                CMrow += [lij]
            CM += [CMrow]

        CMinv = np.linalg.inv(CM)

        rmax = sqrt(-0.5 * CMinv[0][0])
        alpharmax = list(CMinv[0][1:self.dimension+2])

        return (lmin, lmin / lmax, cosmin, nodemin, nodemax, lmin / rmax, alpharmax)
            
##
##  Add n vertices
##
    def add_vertices(self, vertices):
        added_simplex_ids = SortedList(duplicate=False)
        removed_simplex_ids = SortedList(duplicate=False)
        for vertex in vertices:
            self.add_vertex(vertex)

            for simplex_id in self.added_simplex_ids:
                added_simplex_ids.insert(simplex_id)
            for simplex_id in self.removed_simplex_ids:
                removed_simplex_ids.insert(simplex_id)
                try:
                    added_simplex_ids.remove(simplex_id)
                except ValueError:
                    pass
            
        self.added_node_ids = range(self.__node_number - len(vertices), self.__node_number)
        self.added_simplex_ids = list(added_simplex_ids)
        self.removed_simplex_ids = list(removed_simplex_ids)

    def __ridge_nodes(self, ridge):
        nodes = self.__simplices[ridge[1]]
        i = self.__adjacency[ridge[1]].index(ridge[0])
        nodes = set(nodes[:i] + nodes[i+1:])
        return nodes

    def __ridge_simplex_node_id_local(self, ridge, simplex_id):
        ridge_nodes = self.__ridge_nodes(ridge)
        simplex_nodes = self.__simplices[simplex_id]
        node = set(simplex_nodes).difference(ridge_nodes)
        node = node.pop()
        node_id_local = simplex_nodes.index(node)
        return node_id_local
        
##
##  Add a vertex
##
    def add_vertex(self, x):
#       find where the vertex is inserted
        simplex_id = self.find_simplex(x)
#
#       find visible simplices and build a "horizon" graph
#
        x_lifted = x + [self.__lf(x), 1.0]
        simplex_queue = [simplex_id]
        visible_simplex_ids = SortedList(duplicate=False)
#       horizon graph consists of ridges (common faces of two simplices)
        ridges = SortedList(duplicate=False)
#       and peaks (common faces of two ridges)
        peaks = {}
        nodes_to_ridges = {}
        while simplex_queue:
            simplex_id = simplex_queue.pop(0)
#           if face is invisible, then continue
            if dot(self.__simplex_plane[simplex_id], x_lifted) <= 0.0:
                continue
#           face is visible
            visible_simplex_ids.insert(simplex_id)
#           compute outer and inner ridges
            local_inner_ridges = []
            local_outer_ridges = []
            for i in xrange(self.dimension+1):
                simplex_id_neighbor = self.__adjacency[simplex_id][i]
                ridge = [simplex_id, simplex_id_neighbor]; ridge.sort(); ridge = tuple(ridge)
                try:
                    visible_simplex_ids.search(simplex_id_neighbor)
                except ValueError:
                    local_outer_ridges += [ridge]
                    if simplex_id_neighbor > 0:
                        try:
                            simplex_queue.index(simplex_id_neighbor)
                        except ValueError:
                            simplex_queue += [simplex_id_neighbor]
                else:
                    local_inner_ridges += [ridge]

#           remove inner ridges
            for ridge in local_inner_ridges:
                ridges.remove(ridge)
#           add outer ridges
            for ridge in local_outer_ridges:
                ridges.insert(ridge)   

#           update nodes to ridges
            for outer_ridge in local_outer_ridges:
                outer_ridge_nodes = self.__ridge_nodes(outer_ridge)
                for node_id in outer_ridge_nodes:
                    if nodes_to_ridges.has_key(node_id):
                        nodes_to_ridges[node_id] += [outer_ridge]
                    else:
                        nodes_to_ridges[node_id] = [outer_ridge]
            for inner_ridge in local_inner_ridges:
                inner_ridge_nodes = self.__ridge_nodes(inner_ridge)
                for node_id in inner_ridge_nodes:
                    nodes_to_ridges[node_id].remove(inner_ridge)

#
#       compute peaks of the horizon graph
#
        npeaks = 0
        for ridge1 in ridges:
            ridge1_nodes = self.__ridge_nodes(ridge1)
            peaks[ridge1] = []
            for node_id in ridge1_nodes:
                for ridge2 in nodes_to_ridges[node_id]:
                    ridge2_nodes = self.__ridge_nodes(ridge2)
                    peak_nodes = set.intersection(ridge1_nodes, ridge2_nodes)
                    if len(peak_nodes) == self.dimension - 1:
                            peaks[ridge1] += [ridge2]
                            npeaks += 1
#
#       at this point visible simplices and horizon graph are computed
#
        nvisible = len(visible_simplex_ids)
        if nvisible > self.max_visible:
            self.max_visible = nvisible
        nridges = len(ridges)
        if nridges > self.max_ridges:
            self.max_ridges = nridges
        npeaks /= 2
        if npeaks > self.max_peaks:
            self.max_peaks = npeaks

#       add a vertex
        inserted_node_id = self.__node_number
        self.__nodes += [x]
        self.__neighbors += [self.__simplex_number]
        self.__node_number += 1
        self.added_node_ids = [inserted_node_id]

#       build hyperpyramid
        ridge_indices = {}
        invisible_adjacency_update = []
        for ridge in ridges:
            inserted_simplex_id = self.__simplex_number
            self.__simplex_number += 1
            ridge_indices[ridge] = inserted_simplex_id
            visible_simplex_id = ridge[0]
            invisible_simplex_id = ridge[1]
            try:
                visible_simplex_ids.search(visible_simplex_id)
            except ValueError:
                visible_simplex_id = ridge[1]
                invisible_simplex_id = ridge[0]
            deleted_node_id_local = self.__ridge_simplex_node_id_local(ridge, visible_simplex_id)
            simplex = list(self.__simplices[visible_simplex_id])
            simplex[deleted_node_id_local] = inserted_node_id
            self.__simplices += [simplex]

            adjacency = [- (self.dimension + 2)] * (self.dimension + 1)
            adjacency[deleted_node_id_local] = invisible_simplex_id
            self.__adjacency += [adjacency]
            if invisible_simplex_id >= 0:
                node_id_local = self.__ridge_simplex_node_id_local(ridge, invisible_simplex_id)
                invisible_adjacency_update += [(invisible_simplex_id, node_id_local, inserted_simplex_id)]

            self.__children += [[]]

#       build adjacency inside the hyperpyramid
        for ridge_1 in ridges:
            ridge_1_nodes = self.__ridge_nodes(ridge_1)
            simplex_id1 = ridge_indices[ridge_1]
            for ridge_2 in peaks[ridge_1]:
                ridge_2_nodes = self.__ridge_nodes(ridge_2)
                simplex_id2 = ridge_indices[ridge_2]

                node_id1 = ridge_1_nodes.difference(ridge_2_nodes); node_id1 = node_id1.pop()
#                node_2 = ridge_2_nodes.difference(ridge_1_nodes); node_2 = node_2.pop()

                node_id1_local = self.__simplices[simplex_id1].index(node_id1)
#                node_id2_local = self.__simplices[simplex_id2].index(node_2)

                self.__adjacency[simplex_id1][node_id1_local] = simplex_id2
#                self.__adjacency[simplex_id2][node_id2_local] = simplex_id1

#       build adjacency outside of the hyperpyramid
        for update in invisible_adjacency_update:
            invisible_simplex_id, node_id_local, inserted_simplex_id = update
            self.__adjacency[invisible_simplex_id][node_id_local] = inserted_simplex_id
                
#       storing added and removed simplex ids
        self.added_simplex_ids = list(ridge_indices.values())
        self.removed_simplex_ids = list(visible_simplex_ids)


#       clear adjacency lists and create children list
        for simplex_id in visible_simplex_ids:
            self.__adjacency[simplex_id] = [- (self.dimension + 2)] * (self.dimension + 1)
            self.__children[simplex_id] = self.added_simplex_ids

#       compute auxilary data
        for ridge in ridges:
            simplex_id = ridge_indices[ridge]
#           compute neighbors
            for node_id in self.__simplices[simplex_id]:
                self.__neighbors[node_id] = simplex_id
#           compute simplex hyperplane and barycentric coordinates
            X = []
            A = []
            b = []
            for node_id in self.__simplices[simplex_id]:
                X += [self.__nodes[node_id] + [1.0]]
                node_lift = self.__lf(self.__nodes[node_id])
                node = self.__nodes[node_id] + [node_lift, 1.0]
                A += [node]
                b += [0.0]
            A += [[0.0]*self.dimension + [self.__lfmax, 1.0]]
            b += [-1.0]
            n = solve(A, b)
            self.__simplex_plane += [n]
            self.__barycentric += [np.linalg.inv(X).T]
            self.__quality += [self.__compute_quality(simplex_id)]

##
##  verify simplex visibility
##
    
    def __is_simplex_visible(self, simplex_id):
        if self.__children[simplex_id] == []:
            return True
        return False


##  ===============================================
##  ============== I N T E R F A C E ==============
##  ===============================================

##
##  get cartesian number
##

    def cartesian_number(self):
        return 1

##
##  get geometric dimension
##

    def geometric_dimension(self):
        return self.dimension

##
##  get geometric dimensions
##

    def geometric_dimensions(self):
        return [self.dimension]

##
##  get topological dimension
##

    def topological_dimension(self):
        return self.dimension

##
##  get topological dimensions
##

    def topological_dimensions(self):
        return [self.dimension]

##
##  get node number
##

    def node_number(self):
        return self.__node_number

##
##  get simplex number 
##

    def simplex_number(self):
#        return self.__simplex_number                 # WRONG: includes invisible simplices
#        return sum(1 for _ self.simplex_iterator())  # LONG: counts all simplices
        invisible_simplex_number = 0
        simplex_queue = [0]
        while simplex_queue:
            simplex_id = simplex_queue.pop()
            if not self.__is_simplex_visible(simplex_id):
                invisible_simplex_number += 1
                simplex_queue += self.__children[simplex_id]
        return self.__simplex_number - invisible_simplex_number

##
##  get node iterator
##

    def node_iterator(self):
        return xrange(self.__node_number)

##
##  get simplex iterator
##

    def simplex_iterator(self):
        return ifilter(self.__is_simplex_visible, xrange(self.__simplex_number))

##
##  Compute node indecies of a simplex
##

    def node_ids(self, simplex_id):
        if not self.__is_simplex_visible(simplex_id):
            raise MeshError('Cannot extract nodes from meta-simplex')
        return list(self.__simplices[simplex_id])

##
##  Compute node indecies of a simplex so that node_id is at the origin of local coordinates
##

    def node_ids_local(self, simplex_id, node_id):
        simplex = self.node_ids(simplex_id)
        node_id_local = simplex.index(node_id)
        simplex = simplex[node_id_local:] + simplex[:node_id_local]
        return simplex

##
##  Compute the star of a vertex
##

    def star(self, node_id):
        simplex_ids = SortedList(duplicate=False)
        simplex_queue = [self.__neighbors[node_id]]
        while simplex_queue:
            simplex_id = simplex_queue.pop(0)
            simplex_ids.insert(simplex_id)
            for neighbor_id in self.__adjacency[simplex_id]:
                if neighbor_id < 0:
                    continue

                try:
                    simplex_ids.search(neighbor_id)
                except ValueError:
                    pass
                else:
                    continue

                try:
                    self.__simplices[neighbor_id].index(node_id)
                except ValueError:
                    continue
                else:
                    simplex_queue += [neighbor_id]
                        
        return list(simplex_ids)

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates(self, node_id):
        return list(self.__nodes[node_id])


##
##  Compute the geometry of a simplex
##

    def geometry(self, simplex_id):
        return [self.get_coordinates(node_id) for node_id in self.node_ids(simplex_id)]

##
##  quality parameters of a simplex
##

    def get_simplex_quality(self, simplex_id):
        return self.__quality[simplex_id]

##
##  Find simplex
##

    def find_simplex(self, x, simplex_id=None):
        x = np.array(x + [1.0])
        alpha = list(np.dot(self.__barycentric[0], x))                
        if not all([a >= -1.e-8 for a in alpha]):
            raise ValueError("Simplex not found: the point is outside of the domain.")
        simplex_id = 0
        while self.__children[simplex_id]:
            for child_simplex_id in self.__children[simplex_id]:
                alpha = list(np.dot(self.__barycentric[child_simplex_id], x))                
                if all([a >= -1.e-8 for a in alpha]):
                    simplex_id = child_simplex_id
        return simplex_id

##
##  Compute barycentric coordinates
##

    def compute_barycentric(self, x, simplex_id):
        x = np.array(x + [1.0])
        alpha = list(np.dot(self.__barycentric[simplex_id], x))                
        return alpha

class PeriodicHypercubeDelaunayMesh(DelaunayMesh):
    
    def __init__(self, dimension, bounding_box, lifting_function):
        center = vector_sum(bounding_box[0], bounding_box[1])
        center = [0.5 * x for x in center]
        rdius_vector = vector_diff(bounding_box[1],center)
        rdius_vector = [2.0 * x for x in rdius_vector]
        radius =  2.0 * norm(rdius_vector)
        bounding_nodes = bounding_regular_simplex(dimension,center,radius)
        DelaunayMesh.__init__(self, dimension, bounding_nodes, lifting_function)

        self.x0 = vector_diff(center, radius_vector)
        self.x1 = vector_sum(center, radius_vector)
