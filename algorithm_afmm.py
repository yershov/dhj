from utils import PriorityQueue, SortedList
from algorithm import AlgorithmError
from meshtools_markers import PointMarker
import itertools

#from convergence_tools import print_cycle_history

inf = float('inf')

IGNORED = 0
ACCEPTED = 1
CONSIDERED = 2
INCONSISTENT = 3

K_ID = 0
H_ID = 1
V_ID = 2
I_ID = 5
ACI_ID = 6

class AnytimeFastMarchingMethod:

    def __init__(self, mesh, results, system, heuristic, boundary, target, inflation_sequence=None, print_flag=1):
        self.mesh = mesh
        self.results = results
        self.results.node_values_mask = [K_ID, H_ID, V_ID, I_ID, ACI_ID]
        self.results.node_values_names = ['Key', 'H', 'V', 'char_nodes', 'char_weights', 'I', 'aci']
        self.system = system
        self.heuristic = heuristic
        self.boundary = boundary

        self.target = target
        self.target_simplex = mesh.find_simplex(self.target)
        self.target_alpha = [min(max(alpha, 0.0), 1.0) for alpha in mesh.compute_barycentric(self.target, self.target_simplex)]
        self.target_h = self.mesh.get_simplex_quality(self.target_simplex)[0] / self.mesh.get_simplex_quality(self.target_simplex)[1]
        self.target_node_id = None
        self.termination = PointMarker(self.mesh, self.target, 'all')
        self.termination_list = SortedList(self.termination.mark_nodes())
        self.termination_reached = False

        self.inflation_sequence = inflation_sequence

        if self.inflation_sequence == None:
            self.__algorithm_ana = True
        else:
            self.__algorithm_ana = False

        self.considered = PriorityQueue(print_flag)
        self.accepted = SortedList()
        self.inconsistent = SortedList()

        self.V_star = inf
        self.is_V_star_inf = True
        self.K_star = - inf
        
        for (node_id, V) in self.boundary.dirichlet():
            H = self.heuristic(self.mesh.get_coordinates(node_id))
#            key = self.__key(V, H)
#            K = key[0]
#            self.considered.push(key, node_id)
            self.results.addnode(node_id, [inf, H, V, None, None, 0, ACCEPTED])
            self.inconsistent.insert(node_id)

        self.fmm_iteration = 0
        self.n_update = 0

    def __key(self, V, H):
        if self.__algorithm_ana:
            if H == 0:
                K = -1.0
            if self.is_V_star_inf:
                K = -1.0 / (H + self.target_h)
            else:
                K = (V - self.V_star) / (H + self.target_h)
        else:
            K = V + H * self.inflation
        return K

    def __term(self, K_node):
        if self.__algorithm_ana:
            return self.termination_reached
        else:
            return K_node > self.V_star or self.termination_reached


    def fmm_step(self, iteration):
        key, node_id = self.considered.pop()   # lines 57-58
        
        self.accepted.insert(node_id)        # line 58
#
# Update Iterations
#
        values = self.results.getnode(node_id)
        values[I_ID] = iteration
        values[ACI_ID] = ACCEPTED
        self.results.addnode(node_id, values)
        V_node = values[V_ID]

        if self.termination.test_node(node_id):  # line 59
            self.termination_reached = True       # line 61
            if V_node < self.V_star:
                self.V_star = V_node
                self.is_V_star_inf = False
#                return key, node_id
            

        for simplex_id in self.mesh.star(node_id):                   # line 63
            for node_id_neighbor in self.mesh.node_ids(simplex_id):  # line 63
                if node_id_neighbor == node_id:                      # line 63
                    continue                                         # line 63

                if self.boundary.isboundary(node_id_neighbor):
                    continue

                try:
                    self.accepted.search(node_id_neighbor) 
                except ValueError:
                    is_neighbor_accepted = False
                else:
                    is_neighbor_accepted = True
                    
                is_neighbor_source = self.termination.test_node(node_id_neighbor)
#                push_neighbor = not is_neighbor_accepted and is_neighbor_source
                push_neighbor = True
                
                try:
                    values = self.results.getnode(node_id_neighbor)
                except KeyError:
                    V_neighbor = float('inf')
                    I_neighbor = 0
                else:
                    V_neighbor = values[V_ID]
                    I_neighbor = values[I_ID]

                if V_neighbor >= V_node:  # line 64

                    node_ids = list(self.mesh.node_ids_local(simplex_id, node_id_neighbor))

                    nodes = []; V_local = []
                    for node_id_neighbor_2 in node_ids:
                        nodes.append(self.mesh.get_coordinates(node_id_neighbor_2))
                        try:
                            V_neighbor2 = self.results.getnode(node_id_neighbor_2)[V_ID]
                        except KeyError:
                            V_neighbor2 = float('inf')
                        V_local.append(V_neighbor2)
                    

                    V_update, j_dep, beta = self.system.update(nodes, V_local)  # line 65
                    self.n_update += 1
                    if V_neighbor > V_update:   # line 66
                        node_ids_dep = [node_ids[j] for j in j_dep]
                        H_neighbor = self.heuristic(self.mesh.get_coordinates(node_id_neighbor))
                        key_neighbor = self.__key(V_update, H_neighbor)
                        K_neighbor = key_neighbor
 
                        if V_update + H_neighbor <= self.V_star or push_neighbor:   # line 68 

                            if is_neighbor_accepted: # line 69
                                self.inconsistent.insert(node_id_neighbor)          # line 70
                                aci_neighbor = INCONSISTENT
                            else:                 # line 71
                                self.considered.push(key_neighbor, node_id_neighbor)  # line 72
                                aci_neighbor = CONSIDERED

                            values_neighbor = [K_neighbor, H_neighbor, V_update, node_ids_dep, beta, I_neighbor, aci_neighbor]
                            self.results.addnode(node_id_neighbor, values_neighbor)

                            if self.mesh.has_periodic_boundary and self.mesh.is_periodic(node_id_neighbor):
                                for node_id_neighbor_periodic in self.mesh.periodic_connections(node_id_neighbor):
                                    self.results.addnode(node_id_neighbor_periodic, list(values_neighbor))
                                    if is_neighbor_accepted:
                                        self.inconsistent.insert(node_id_neighbor_periodic)
                                    else:
                                        self.considered.push(key_neighbor, node_id_neighbor_periodic)

        return key, node_id


    def improve_solution(self):
        self.fmm_iteration = 0
        self.n_update = 0
        K_node = 0.0
        while not self.__term(K_node):
            if self.considered.is_empty():
                return
#                raise AlgorithmError('Environment is disconnected')

            self.fmm_iteration += 1

            key_node, node_id = self.fmm_step(self.fmm_iteration)
            K_node = key_node

            if K_node > self.K_star: # and K_node < 0.0:
                self.K_star = K_node
                self.n_id_star = node_id


    def step(self):
#
# Update inflation for ARA
#           
        if not self.__algorithm_ana:
            self.inflation = self.inflation_sequence.pop(0)
#
# Update considered with inconsistent nodes   # line 15
#           
        VpH_min = float('inf')
        node_list = []
        while not self.considered.is_empty():
            key, node_id = self.considered.pop()
            node_list.append(node_id)
            values = self.results.getnode(node_id)
            V = values[V_ID]; H = values[H_ID]; VpH = V + H
            if VpH_min > VpH:
                VpH_min = VpH
        node_list.extend(self.inconsistent)

        for node_id in node_list:
            values = self.results.getnode(node_id)
            V = values[V_ID]; H = values[H_ID]
#            if V + H > self.V_star:  # line 16
#                continue
            key = self.__key(V, H)
            K = key
            values[K_ID] = K
            self.results.addnode(node_id, values)
#            if not self.termination.test_node(node_id):
            self.considered.push(key, node_id)
                
        self.inconsistent.clear()
        self.accepted.clear()

#
# Adapt Inflation (Zach's idea)
#           
        if not self.__algorithm_ana:
            epsilon_temp = self.V_star / VpH_min
            while self.inflation >= epsilon_temp:
                self.inflation = self.inflation_sequence.pop(0)
#
# Update order
#           
        for node_id, values in self.results.node_values:
            if values[ACI_ID] == IGNORED or values[ACI_ID] == ACCEPTED:
                values[I_ID] = 0; values[ACI_ID] = IGNORED
                self.results.addnode(node_id, values)
            else:
                values[I_ID] = 0; values[ACI_ID] = CONSIDERED
                self.results.addnode(node_id, values)

#
# Reset termination flag
#
        self.termination_reached = False

#
# Clear termination nodes
#
#        for node_id in self.termination_list:
#            try:
#                self.results.removenode(node_id)
#            except KeyError:
#                pass
#
# Improve solution
#
        self.improve_solution()   # line 14

#
# Update inflation for ANA
#           
        if self.__algorithm_ana:
            self.inflation = - self.K_star


    def plan(self):
        iteration = 0
        is_term = False
        while  not is_term:
            self.step()

            if not self.__algorithm_ana and not self.inflation_sequence:
                is_term = True
            if self.__algorithm_ana and self.considered.is_empty() and self.inconsistent.is_empty():
                is_term = True

            iteration += 1                
            yield iteration
