##
##  Fast Marching with rapidly diminishing numerical error (The ONE)
##
from utils import *
from algorithm import AlgorithmError
from meshtools_markers import IncrementalMarker

#from convergence_tools import print_cycle_history

inf = float('inf')

class REDFMM:

    def __init__(self, mesh, sampling, results, system, boundary, termination, tolerance=None, max_iteration=None, print_flag=1):
        self.mesh = mesh
        self.sampling = sampling
        self.results = results
        self.results.node_values_mask = [0]
        self.results.node_values_names = ['V', 'char_nodes', 'char_weights', 'char_simplex']
        self.system = system
        self.boundary = boundary
        self.termination = termination
        self.tolerance = tolerance
        self.max_iteration = max_iteration
        self.print_flag = print_flag

        self.pq = PriorityQueue(self.print_flag)

        for (node_id, V) in self.boundary.dirichlet():
            if V != float('inf'):
                self.results.addnode(node_id, [V, [], [], None])
                self.pq.push([V, [], [], None], node_id)



    def clear_dependencies(self, ini_node_id):
        dependency_queue = SortedList([ini_node_id], duplicate=False)
        cleared_node_ids = SortedList(duplicate=False)

        while dependency_queue:
#            print "dependency_queue", dependency_queue
            node_id = dependency_queue.pop()
#            print "node_id", node_id
            try:
                self.pq.remove(node_id)
            except ValueError:
                pass
            try:
                self.results.removenode(node_id)
            except KeyError:
                pass
#            print "star", self.mesh.star(node_id)
            for simplex_id in self.mesh.star(node_id):
#                print "simplex_id", simplex_id
                for node_id_neighbor in self.mesh.node_ids(simplex_id):
                    if node_id_neighbor == node_id:
                        continue
#                    print "node_id_neighbor", node_id_neighbor
                    try:
                        results_neighbor = self.results.getnode(node_id_neighbor)
                    except KeyError:
                        continue
#                    print "results_neighbor[1]", results_neighbor[1]
                    try:
                        results_neighbor[1].index(node_id)
                    except ValueError:
                        continue
                    dependency_queue.insert(node_id_neighbor)

            cleared_node_ids.insert(node_id)

        for node_id in cleared_node_ids:
            self.update_node(node_id)


    def update_samples(self, added_node_ids, removed_simplex_ids, added_simplex_ids):
        self.pq.clear()

        self.boundary.marker.add_nodes(added_node_ids)
        self.boundary.marker.remove_simplices(removed_simplex_ids)
        self.boundary.marker.add_simplices(added_simplex_ids)

        self.termination.add_nodes(added_node_ids)
        self.termination.remove_simplices(removed_simplex_ids)
        self.termination.add_simplices(added_simplex_ids)

        self.termination_nodes = self.termination.mark_nodes()

        node_ids = SortedList(added_node_ids, duplicate=False)
        for node_id in self.termination_nodes:
            node_ids.insert(node_id)
        for simplex_id in added_simplex_ids:
            for node_id in self.mesh.node_ids(simplex_id):
                node_ids.insert(node_id)

        for node_id in node_ids:
            try:
                self.results.removenode(node_id)
            except KeyError:
                pass

            if self.boundary.isboundary(node_id):
                V = self.boundary.g(self.mesh.get_coordinates(node_id))
                if V != float('inf'):
                    self.results.addnode(node_id, [V, [], [], None])
                    self.pq.push([V, [], [], None], node_id)
            else:
                self.update_node(node_id)


    def update_node(self, node_id):
        V_min = float('inf')
        node_ids_dep_min = []
        beta_min = []
        simplex_id_min = None
#        print "update_node", node_id, self.mesh.star(node_id)
        for simplex_id in self.mesh.star(node_id):
            nodes = []; V = []
            node_ids = self.mesh.node_ids_local(simplex_id, node_id)
            for node_id_neighbor in node_ids:
                nodes += [self.mesh.get_coordinates(node_id_neighbor)]
                try:
                    V_node = self.results.getnode(node_id_neighbor)[0]
                except KeyError:
                    V_node = float('inf')
                V += [V_node]
#            print simplex_id, nodes, V
            V_node, j_dep, beta = self.system.update(nodes, V)
#            print V_node, j_dep, beta
            if V_node < V_min:
                V_min = V_node
                node_ids_dep_min = [node_ids[j] for j in j_dep]
                beta_min = beta
                simplex_id_min = simplex_id

        if V_min != float('inf'):
            self.results.addnode(node_id, [V_min, node_ids_dep_min, beta_min, simplex_id_min])
            self.pq.push([V_min, node_ids_dep_min, beta_min, simplex_id_min], node_id)
#            print "update_node", self.pq
            

    def fmm_step(self):
#        print "-"*200
        results_node, node_id = self.pq.pop()
                
        for simplex_id in self.mesh.star(node_id):
            for node_id_neighbor in self.mesh.node_ids(simplex_id):
                if node_id_neighbor == node_id:
                    continue
                if self.boundary.isboundary(node_id_neighbor):
                    continue

                node_ids = self.mesh.node_ids_local(simplex_id, node_id_neighbor)

                nodes = []; V = []
                for node_id_neighbor_2 in node_ids:
                    nodes += [self.mesh.get_coordinates(node_id_neighbor_2)]
                    try:
                        V_node = self.results.getnode(node_id_neighbor_2)[0]
                    except KeyError:
                        V_node = float('inf')
                    V += [V_node]

                V_neighbor, j_dep, beta = self.system.update(nodes, V)
                node_ids_dep = [node_ids[j] for j in j_dep]

                try:
                    results_neighbor = self.results.getnode(node_id_neighbor)
                except KeyError:
                    results_neighbor = [float('inf'), [], [], None]

                if V_neighbor < results_neighbor[0] * (1.0 - self.tolerance):
                    self.results.addnode(node_id_neighbor, [V_neighbor, node_ids_dep, beta, simplex_id])
                    self.pq.push([V_neighbor, node_ids_dep, beta, simplex_id], node_id_neighbor)
                elif V_neighbor > results_neighbor[0] * (1.0 + self.tolerance) and simplex_id == results_neighbor[3]:
                    self.clear_dependencies(node_id_neighbor)

        return results_node, node_id

    def fmm_plan(self):
        is_term = False
        while not is_term:
#            print "="*200
#            print "fmm_plan", self.pq
            try:
                results_node, node_id = self.fmm_step()
            except IndexError:
                break
#            print "-"*200
            if self.termination.test_node(node_id):
                is_term = True
                for node_id in self.termination.mark_nodes():
                    try:
                        values  = self.results.getnode(node_id)
                    except KeyError:
                        is_term = False

        # if not is_term:
        #     for node_id in self.termination.mark_nodes():
        #         results = self.update_node(node_id)
        #         self.results.addnode(node_id, results)


    def plan(self):
        iteration = 0
        is_term = False
        while not is_term:
            self.sampling.sample()
            self.update_samples(self.mesh.added_node_ids, self.mesh.removed_simplex_ids, self.mesh.added_simplex_ids)
            self.fmm_plan()
            if self.max_iteration != None and iteration >= self.max_iteration:
                is_term = True
            iteration += 1
            yield iteration

