from math import sqrt, isinf
import numpy as np
import itertools
from utils import *
from system import SystemError

class OmniVehicle2D(object):

    def __init__(self, mesh, speed_function = (lambda x: 1.0), weight_function = (lambda x: 1.0)):
        self.geometric_dimension = mesh.geometric_dimension()
        self.topological_dimension = mesh.topological_dimension()
        if self.topological_dimension != 2:
            raise SystemError('Implemented for 2D meshes only')
        self.weight_function = weight_function
        self.speed_function = speed_function


    def update(self, x, V):
        x0 = x[0]
        speed = self.speed_function(x0)
        if speed == 0.0:
            return float('inf'), [], []
        weight = self.weight_function(x0)
        if weight == float('inf'):
            return float('inf'), [], []

        c = weight / speed

        V1 = V[1]
        V2 = V[2]

        x1 = [x[1][i] - x[0][i] for i in xrange(self.geometric_dimension)]
        x2 = [x[2][i] - x[0][i] for i in xrange(self.geometric_dimension)]

        P11 = sum([x1[i]**2 for i in xrange(self.geometric_dimension)])
        P12 = sum([x1[i]*x2[i] for i in xrange(self.geometric_dimension)])
        P22 = sum([x2[i]**2 for i in xrange(self.geometric_dimension)])

        if isinf(V1):
            if isinf(V2):
                return float('inf'), [], []
            return V2 + sqrt(P22) * c, [2], [1.0]
        elif isinf(V2):
            return V1 + sqrt(P11) * c, [1], [1.0]
        
        det = P11 * P22 - P12**2

        PI11 =   P22 / det
        PI12 = - P12 / det
        PI22 =   P11 / det

        A = PI11 + 2 * PI12 + PI22
        B = (V1 * PI11 + (V1 + V2) * PI12 + V2 * PI22)
        C = V1**2 * PI11 + 2 * V1 * V2 * PI12 + V2**2 * PI22 - c**2

#       solve A v0^2 - 2 B v0 + C = 0
        D = B**2 - A*C
        if D < 0.0:
            V1 = V1 + sqrt(P11) * c
            V2 = V2 + sqrt(P22) * c
            if V1 < V2:
                return V1, [1], [1.0]
            return V2, [2], [1.0]
        
        V0 = (B + sqrt(D)) / A

        beta1 = (V1 - V0) * PI11 + (V2 - V0) * PI12
        beta2 = (V1 - V0) * PI12 + (V2 - V0) * PI22

        if beta1 > 0.0 or beta2 > 0.0:
            if beta1 > 0.0:
                return V2 + sqrt(P22) * c, [2], [1.0]
            V0 = V1 + sqrt(P11) * c
            return V0, [1], [1.0]

        beta_sum = beta1 + beta2
        beta = [beta1 / beta_sum, beta2 / beta_sum]

        return V0, [1,2], beta
