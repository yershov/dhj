import copy
import numpy as np
from utils import *
from math import sqrt
import itertools
from scipy.spatial import Delaunay
from mesh import MeshError


class DelaunayMesh:
    
    def __init__(self, dimension, lifting_function):
        self.dimension = dimension
        self.init_points = []
        self.triangulation = None


##
##  get cartesian number
##

    def cartesian_number(self):
        return 1

##
##  get node number
##

    def node_number(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        if self.triangulation == None:
            return 0 ## maybe return len(self.init_points)
        else:
            return self.triangulation.npoints

##
##  get simplex number
##

    def simplex_number(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        if self.triangulation == None:
            return 0
        else:
            return self.triangulation.nsimplex

##
##  get geometric dimension
##

    def geometric_dimension(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.dimension

##
##  get topological dimension
##

    def topological_dimension(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.dimension

##
##  Extract simplex from a Delaunay mesh
##
    
    def get_simplex(self, simplex_id):
        simplex_id = simplex_id[0]
        return [list(self.triangulation.simplices[simplex_id])]

##
##  Extract simplex from a cartesian mesh such that node_id is at the origin of a local coorduinate system
##

    def get_simplex_local(self, simplex_id, node_id):
        node_id = node_id[0]
        simplex = self.get_simplex(simplex_id)[0]
        node_id_local = simplex.index(node_id)
        simplex = simplex[node_id_local:] + simplex[:node_id_local]
        return [simplex]

##
##  Extract node neighbors from a cartesian mesh
##

    def get_neighbors(self, node_id):
        node_id = node_id[0]
        simplex_ids = SortedList(duplicate=False)
        simplex_queue = [self.triangulation.vertex_to_simplex[node_id]]
        while simplex_queue:
            simplex_id = simplex_queue.pop(0)
            simplex_ids.insert(simplex_id)
            for neighbor_id in self.triangulation.neighbors[simplex_id]:
                if neighbor_id == -1:
                    continue

                try:
                    simplex_ids.search(neighbor_id)
                    continue
                except ValueError:
                    pass

                node_ids = list(self.triangulation.simplices[neighbor_id])

                try:
                    node_ids.index(node_id)
                except ValueError:
                    continue
                else:
                    simplex_queue += [neighbor_id]
                        
        return [list(simplex_ids)]

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates(self, node_id):
        node_id = node_id[0]
        return self.triangulation.points[node_id].tolist()

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates_projected(self, mesh_id, node_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.get_coordinates([node_id])

##
##  Ads n points
##
    def add_points(self, points):
        if self.init_points == None:
            self.triangulation.add_points(points)
        else:
            self.init_points += points
            if len(self.init_points) >= self.dimension + 2:
                self.triangulation = Delaunay(self.init_points,incremental=True)
                self.init_points = None

##
##  Precompute neighbours of a vertex.
##

    def compute_neighbors(self):
        raise MeshError('compute_neighbors is not implemented for Delaunay triangulations')
        

##
##  Precompute simplicial barycentric coordinates.
##

    def precompute_barycentric(self):
        raise MeshError('precompute_barycentric is not implemented for Delaunay triangulations')

##
##  Precompute mesh quality.
##

    def precompute_quality(self):
        raise MeshError('precompute_quality is not implemented for Delaunay triangulations')

    def quality(self, mesh_id, simplex_id):
        raise MeshError('quality is not implemented for Delaunay triangulations')

##
##  Find simplex
##

    def find_simplex(self, x, simplex_id=None):
        if self.triangulation == None:
            raise ValueError("Simplex not found: the point is outside of the domain.")            
        simplex_id = self.triangulation.find_simplex([x]).tolist()
        if simplex_id[0] == -1:
            raise ValueError("Simplex not found: the point is outside of the domain.")
        return [simplex_id[0]]

##
##  Compute the star of a vertex
##

    def star(self, node_id):
        return [(simplex_id, ) for simplex_id in self.get_neighbors(node_id)[0]]

##
##  Compute the geometry of a simplex
##

    def geometry(self, simplex_id):
        return [self.get_coordinates(node_id) for node_id in self.node_ids(simplex_id)]

##
##  Compute node indecies of a simplex
##

    def node_ids(self, simplex_id):
        return [(node_id, ) for node_id in self.get_simplex(simplex_id)[0]]

##
##  Compute projected node indecies of a projected simplex onto mesh_id
##

    def node_ids_projected(self, mesh_id, simplex_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return [node_id[0] for node_id in self.node_ids([simplex_id])]

##
##  Compute node indecies of a simplex so that node_id is at the origin of local coordinates
##

    def node_ids_local(self, simplex_id, node_id):
        return [(node_id_local,) for node_id_local in self.get_simplex_local(simplex_id, node_id)[0]]
