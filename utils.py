import heapq 
import bisect
from math import sqrt
import numpy as np


def dot(x,y):
    return sum(map(lambda a,b: a*b, x, y))

def norm1(x):
    return sum(map(lambda a: abs(a), x))

def norm2(x):
    return sqrt(dot(x,x))

def norm0(x):
    return max(map(lambda a: abs(a), x))

def norm(x):
    return norm2(x)

def vector_diff(x,y):
    return map(lambda a,b: a-b, x, y)

def vector_sum(x,y):
    return map(lambda a,b: a+b, x, y)

def vector_scale(a,x):
    return map(lambda b: a*b, x)

def prod(iterable):
    return reduce(lambda a, b: a*b, iterable, 1.0)

def null_space(x, tolerance=1.e-8):
    X = np.array(x)
    u, s, vh = np.linalg.svd(X)
    nnz = (s >= tolerance).sum()
    ns = vh[nnz:].conj()
    result = []
    for vec in ns:
        result += [list(vec)]
    return result

def solve(A, b):
    return list(np.linalg.solve(A, b))


class PriorityQueue:

    def __init__(self, print_flag=0):
        self.__pq = []         # the priority queue list
        self.__map = {}        # mapping of nodes to entries
        self.__val = {}        # mapping of nodes to entries
        self.__p = print_flag  # print flag

    def __str__(self):
        res = "["
        for q_element in self.__pq:
            if q_element[2]:
                res += "{" + str(q_element[0]) + " : " + str(q_element[1]) + "} "
        res.strip(); res += "]"
        return res

    def is_empty(self):
        isempty = True
        for q_element in self.__pq:
           if q_element[2]:
               isempty = False
               break
        return isempty;

    def clear(self):
        self.__pq = []
        self.__map = {}
        self.__val = {}

    def __len__(self):
        len_ = 0
        for el in self.__pq:
            if el[2]:
                len_ += 1
        return len_

    def push(self, key, obj):
        if self.__map.has_key(obj):
            if key < self.__val[obj]:
                self.__setPriority(key, obj)
                if self.__p / 2:
                    print "!--", key, obj
            if not self.__map[obj][2]:
                self.__add(key, obj)
                if self.__p / 2:
                    print "<--", key, obj
        else:
            self.__add(key, obj)
            if self.__p / 2:
                print "<--", key, obj

    def pop(self):
        while self.__pq:
            key, obj, VALID = heapq.heappop(self.__pq)
            if VALID:
                del self.__map[obj]
                del self.__val[obj]
                if self.__p:
                    print "-->", key, obj
                return key, obj
        raise IndexError('Priority queue is empty')

    def has_item(self, obj):
        return self.__map.has_key(obj) and self.__map[obj][2]

    def remove(self, obj):
        if self.__map.has_key(obj):
            if self.__p / 2:
                key = self.__val[obj]
                print "x--", key, obj
            self.__delete(obj)
        else:
            raise ValueError('Object ' + str(obj) + ' is not found')

    def __add(self, key, obj):
        q_element = [key, obj, True]
        self.__map[obj] = q_element
        self.__val[obj] = key
        heapq.heappush(self.__pq, q_element)

    def __delete(self, obj):
        q_element = self.__map[obj]
        q_element[2] = False

    def __setPriority(self, key, obj):
        self.__delete(obj)
        self.__add(key, obj)


class SortedList:
    def __init__(self, l=None, duplicate=True):
        self.__list = []
        if l != None:
            self.__list = list(l)
            self.__list.sort()
        self.__duplicate = duplicate

    def __str__(self):
        return str(self.__list)

    def __len__(self):
        return len(self.__list)

    def __iter__(self):
        return iter(self.__list)

    def __getitem__(self, index):
        return self.__list[index]

    def is_empty(self):
        if self.__list:
            return False
        return True

    def clear(self):
        self.__list = []

    def pop(self, i=-1):
        return self.__list.pop(i)

    def insert(self, value):
        if self.__duplicate:
            bisect.insort(self.__list, value)
        else:
            try:
                index = self.search(value)
            except ValueError:
                bisect.insort(self.__list, value) 
               

    def remove(self, value):
        if self.__duplicate:
            index = self.search(value)
            while True:
                del self.__list[index]
                try:
                    index = self.search(value)
                except ValueError:
                    break
        else:
            index = self.search(value)
            del self.__list[index]


    def search(self, value):
        index = bisect.bisect_left(self.__list, value)
        if index < len(self.__list) and self.__list[index] == value:
            return index
        raise ValueError('Ordered list has no ' + str(value))


class SortedDictionary:
    def __init__(self, d=None):
        self.__list = []
        self.__pointers = {}
        if d != None:
            self.__list = sorted([(el[1], el[0]) for el in d.items()])
            self.__pointers = {self.__list[i][1]: i for i  in xrange(len(self.__list))}

    def __str__(self):
        return ", ".join([str(el[1]) + ': ' + str(el[0]) for el in self.__list])

    def __len__(self):
        return len(self.__list)

    def __iter__(self):
        return iter(self.__list)

    def __getitem__(self, index):
        return self.__list[index]

    def __update_pointers(self, index):
        for i in xrange(index, len(self.__list)):
            key = self.__list[i][1]
            self.__pointers[key] = i
        
    def is_empty(self):
        if self.__list:
            return False
        return True

    def push(self, key, value):
        if self.__pointers.has_key(key):
            self.remove(key)
        index = bisect.bisect_left(self.__list, (value, key))
        self.__list.insert(index, (value, key))
        self.__update_pointers(index)

    def remove(self, key):
        index = self.__pointers[key]
        del self.__list[index]
        del self.__pointers[key]
        self.__update_pointers(index)

    def value(self, key):
        index = self.__pointers[key]
        return self.__list[index][0]

    def max_key(self):
        return self.__list[-1][1]

    def min_key(self):
        return self.__list[0][1]
