import copy
import numpy as np
from utils import *
from math import sqrt
import itertools
from mesh import MeshError
from vtk_writer import write_vtu


def bounding_regular_simplex(d,c,r):
    x0 = (1.0 - sqrt(1.0 + d)) / d
    bc = (1.0 + x0) / (d + 1)
    r0 = - ( bc - 1.0 + bc * (d-1)) / sqrt(d)
    x = [map(lambda ci: (x0 - bc) * r / r0 + ci, c)]
    for i in xrange(d):
        e = [0.0]*d; e[i] = 1.0
        xi = map(lambda xi, ci: (xi - bc) * r / r0 + ci, e, c)
        x += [xi]
    return x
    

class DelaunayMesh:
    
    def __init__(self, dimension, bounding_nodes, lifting_function):
        self.dimension = dimension
        if len(bounding_nodes) != dimension+1:
            raise MeshError('Number of bounding nodes must be exactly ' + str(dimension+1) + ', but ' + str(len(bounding_nodes)) + ' are given')

        self.__lf = lifting_function

        self.__nodes = []
        self.__lfmax = 0.0
        X = []
        A = []
        b = []
        for node in bounding_nodes:
            self.__nodes += [node]
            node_lf = lifting_function(node)
            if node_lf > self.__lfmax:
                self.__lfmax = node_lf

            X += [node + [1.0]]
            a = node + [node_lf, 1.0]
            A += [a]
            b += [0.0]

        A += [[0.0]*self.dimension + [self.__lfmax, 1.0]]
        b += [-1.0]
        n = solve(A, b)
#        if len(n) != 1:
#            raise MeshError('Bounding nodes are not in general position')
#        n = n[0]

        for node in bounding_nodes:
            node_lf = lifting_function(node)
            a = node + [node_lf, 1.0]
        

        self.__simplex_plane = [n]
 
        self.__simplices = [range(self.dimension+1)]

        self.__children = [[]]
        self.__adjacency = [range(-1,-(self.dimension+2),-1)]

        self.__neighbors = [0]*(self.dimension+1)

        self.__barycentric = [np.linalg.inv(X).T]

        quality = self.__compute_quality(0)
        self.__quality = [quality]
        self.__suggested_refinement = PriorityQueue()
        self.__suggested_refinement.push(quality[5], 0)

        self.max_visible = 0
        self.max_ridges = 0
        self.max_peaks = 0

##
##  get cartesian number
##

    def cartesian_number(self):
        return 1

##
##  get node number
##

    def node_number(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return len(self.__nodes)

##
##  get simplex number
##

    def simplex_number(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return len(self.__simplices)

##
##  get geometric dimension
##

    def geometric_dimension(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.dimension

##
##  get topological dimension
##

    def topological_dimension(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.dimension

##
##  Extract simplex from a Delaunay mesh
##
    
    def get_simplex(self, simplex_id):
        simplex_id = simplex_id[0]
        return [list(self.__simplices[simplex_id])]

##
##  Extract simplex from a cartesian mesh such that node_id is at the origin of a local coorduinate system
##

    def get_simplex_local(self, simplex_id, node_id):
        node_id = node_id[0]
        simplex = self.get_simplex(simplex_id)[0]
        node_id_local = simplex.index(node_id)
        simplex = simplex[node_id_local:] + simplex[:node_id_local]
        return [simplex]

##
##  Extract node neighbors from a cartesian mesh
##

    def get_neighbors(self, node_id):
        node_id = node_id[0]
        simplex_ids = SortedList(duplicate=False)
        simplex_queue = [self.__neighbors[node_id]]
        while simplex_queue:
            simplex_id = simplex_queue.pop(0)
            simplex_ids.insert(simplex_id)
            for neighbor_id in self.__adjacency[simplex_id]:
                if neighbor_id < 0:
                    continue

                try:
                    simplex_ids.search(neighbor_id)
                except ValueError:
                    pass
                else:
                    continue

                try:
                    self.__simplices[neighbor_id].index(node_id)
                except ValueError:
                    continue
                else:
                    simplex_queue += [neighbor_id]
                        
        return [list(simplex_ids)]

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates(self, node_id):
        node_id = node_id[0]
        return list(self.__nodes[node_id])

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates_projected(self, mesh_id, node_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.get_coordinates([node_id])

##
##  Add n points
##
    def add_points(self, points):
        for point in points:
            self.add_point(point)


    def __compute_quality(self, simplex_id):
        nodes = self.__simplices[simplex_id]
        x = [self.__nodes[node_id] for node_id in nodes]
        lmin = float('inf')
        lmax = 0.0
        cosmin = float('inf')
        for x1 in x:
            for x2 in x:
                if x2 == x1:
                    continue
                x12 = vector_diff(x1,x2)
                l = norm(x12)
                if l > lmax:
                    lmax = l
                if l < lmin:
                    lmin = l
                for x3 in x:
                    if x3 == x1 or x3 == x2:
                        continue
                    x32 = vector_diff(x3,x2)
                    cos = dot(x12,x32) / l / norm(x32)
                    if cos < cosmin:
                        cosmin = cos
                    
        CM = [[0] + [1] * (self.dimension+1)]
        for i in xrange(self.dimension+1):
            CMrow = [1]
            for j in xrange(self.dimension+1):
                xij = vector_diff(x[i], x[j])
                lij = dot(xij, xij)
                CMrow += [lij]
            CM += [CMrow]

        CMinv = np.linalg.inv(CM)

        rmax = sqrt(-0.5 * CMinv[0][0])
        alpharmax = list(CMinv[0][1:self.dimension+2])

        return (lmin, lmin / lmax, cosmin, (), (), lmin / rmax, alpharmax)
            


    def __ridge_nodes(self, ridge):
        nodes = self.__simplices[ridge[1]]
        i = self.__adjacency[ridge[1]].index(ridge[0])
        nodes = set(nodes[:i] + nodes[i+1:])
        return nodes

    def __ridge_simplex_node_id_local(self, ridge, simplex_id):
        ridge_nodes = self.__ridge_nodes(ridge)
        simplex_nodes = self.__simplices[simplex_id]
        node = set(simplex_nodes).difference(ridge_nodes)
        node = node.pop()
        node_id_local = simplex_nodes.index(node)
        return node_id_local

    def __temp_vtu(self):
        if self.dimension == 1:
            case = 3
        elif self.dimension == 2:
            case = 5
        elif self.dimension == 3:
            case = 10
        else:
            return
        nodes = []
        cells = []
        node_map = {}
        node_counter = 0
        for simplex_id in self.__temp_simplices:
            cell = []
            for node_id in self.__simplices[simplex_id]:
                try:
                    index = node_map[node_id]
                except KeyError:
                    nodes += [self.__nodes[node_id]]
                    node_map[node_id] = node_counter
                    index = node_counter
                    node_counter += 1
                cell += [index]
            cells += [cell]

        write_vtu(Verts=np.array(nodes), 
                  Cells={case:np.array(cells)},
                  fname='temp'+str(self.__temp_counter)+'.vtu')
        self.__temp_counter += 1
        
##
##  Add a point
##
    def add_point(self, x):
#       find where point is inserted
        simplex_id = self.find_simplex(x)[0]
#        print "="*120
#        print x, " located in", simplex_id
#        print "adjacency before rewiering"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120
#
#       find visible simplices and build a "horizon" graph
#
        x_lifted = x + [self.__lf(x), 1.0]
        simplex_queue = [simplex_id]
        visible_simplex_ids = SortedList(duplicate=False)
#       horizon graph consists of ridges (common faces of two simplices)
        ridges = SortedList(duplicate=False)
#       and peaks (common faces of two ridges)
        peaks = {}
        nodes_to_ridges = {}
#        self.__temp_counter = 0
#        self.__temp_simplices = []
        while simplex_queue:
            simplex_id = simplex_queue.pop(0)
#            print "simplex_id", simplex_id
#            print "adjacency[simplex_id]", self.__adjacency[simplex_id]
#           if face is invisible, then continue
            if dot(self.__simplex_plane[simplex_id], x_lifted) <= 0.0:
                continue
#           face is visible
            visible_simplex_ids.insert(simplex_id)
#           compute outer and inner ridges
            local_inner_ridges = []
            local_outer_ridges = []
#            print "visible_simplex_ids", visible_simplex_ids
            for i in xrange(self.dimension+1):
                simplex_id_neighbor = self.__adjacency[simplex_id][i]
                ridge = [simplex_id, simplex_id_neighbor]; ridge.sort(); ridge = tuple(ridge)
                try:
                    visible_simplex_ids.search(simplex_id_neighbor)
                except ValueError:
                    local_outer_ridges += [ridge]
                    if simplex_id_neighbor > 0:
                        try:
                            simplex_queue.index(simplex_id_neighbor)
                        except ValueError:
                            simplex_queue += [simplex_id_neighbor]
                else:
                    local_inner_ridges += [ridge]
#            print "simplex_queue", simplex_queue
#           compute new peaks
            outer_ridges_number = len(local_outer_ridges)
#            print "local_outer_ridges", local_outer_ridges
#            print "local_inner_ridges", local_inner_ridges
            for outer_ridge_id1 in xrange(outer_ridges_number):
                outer_ridge_1 = local_outer_ridges[outer_ridge_id1]
                outer_ridge_1_nodes = self.__ridge_nodes(outer_ridge_1)
                peaks[outer_ridge_1] = []
#               outer ridges are all connected
                for outer_ridge_id2 in xrange(outer_ridge_id1):
                    outer_ridge_2 = local_outer_ridges[outer_ridge_id2]
                    peaks[outer_ridge_1] += [outer_ridge_2]
                    peaks[outer_ridge_2] += [outer_ridge_1]
#               connect outer ridges to the previous ridges through inner ones
                for node_id in outer_ridge_1_nodes:
                    if not nodes_to_ridges.has_key(node_id):
                        continue
                    for outer_ridge_3 in nodes_to_ridges[node_id]:
                        try:
                            local_inner_ridges.index(outer_ridge_3)
                            continue
                        except ValueError:
                            pass
                        outer_ridge_3_nodes = self.__ridge_nodes(outer_ridge_3)
                        peak_nodes = set.intersection(outer_ridge_1_nodes, outer_ridge_3_nodes)
                        if len(peak_nodes) == self.dimension - 1:
#                            print "peak added between", outer_ridge_1, "and", outer_ridge_3, "through", node_id
                            peaks[outer_ridge_1] += [outer_ridge_3]
                            peaks[outer_ridge_3] += [outer_ridge_1]

#           remove old peaks
            for inner_ridge in local_inner_ridges:
                for outer_ridge in peaks[inner_ridge]:
                    peaks[outer_ridge].remove(inner_ridge)
                peaks[inner_ridge] = []

#            print "peaks", peaks

#           update nodes to ridges
            for outer_ridge in local_outer_ridges:
                outer_ridge_nodes = self.__ridge_nodes(outer_ridge)
                for node_id in outer_ridge_nodes:
                    if nodes_to_ridges.has_key(node_id):
                        nodes_to_ridges[node_id] += [outer_ridge]
                    else:
                        nodes_to_ridges[node_id] = [outer_ridge]
                
            for inner_ridge in local_inner_ridges:
                inner_ridge_nodes = self.__ridge_nodes(inner_ridge)
                for node_id in inner_ridge_nodes:
                    nodes_to_ridges[node_id].remove(inner_ridge)

#           remove inner ridges
            for ridge in local_inner_ridges:
                ridges.remove(ridge)
#           add outer ridges
            for ridge in local_outer_ridges:
                ridges.insert(ridge)   

#            self.__temp_simplices += [simplex_id] 
#            self.__temp_vtu()

            # for i1 in range(1,len(ridges)):
            #     ridge_1 = ridges[i1]
            #     ridge_1_nodes = self.__ridge_nodes(ridge_1)
            #     for i2 in range(i1):
            #         ridge_2 = ridges[i2]
            #         ridge_2_nodes = self.__ridge_nodes(ridge_2)
            #         peak_nodes = set.intersection(ridge_1_nodes, ridge_2_nodes)
            #         if len(peak_nodes) == self.dimension - 1:
            #             try:
            #                 peaks[ridge_1].index(ridge_2)
            #                 peaks[ridge_2].index(ridge_1)
            #             except ValueError:
            #                 print "peaks between", ridge_1, "and", ridge_2, "are missing"

            # for _, peak in peaks.items():
            #     if len(peak) < self.dimension and len(peak) != 0:
            #         print peak
            #         print "peaks problem"
            #         exit(0)
#            print "-"*120
#
#       at this point visible simplices and horizon graph are computed
#
        nvisible = len(visible_simplex_ids)
        if nvisible > self.max_visible:
            self.max_visible = nvisible
        nridges = len(ridges)
        if nridges > self.max_ridges:
            self.max_ridges = nridges
#        npeaks /= 2
#        if npeaks > self.max_peaks:
#            self.max_peaks = npeaks

#        print "visible simplices", visible_simplex_ids
#        print "ridges", ridges
#        print "peaks", peaks

#       add node
        inserted_node_id = len(self.__nodes)
        self.__nodes += [x]
        self.__neighbors += [len(self.__simplices)]
        
#       build hyperpyramid
        ridge_indices = {}
        invisible_adjacency_update = []
        for ridge in ridges:
#            print "ridge", ridge
            inserted_simplex_id = len(self.__simplices)
            ridge_indices[ridge] = inserted_simplex_id
            visible_simplex_id = ridge[0]
            invisible_simplex_id = ridge[1]
            try:
                visible_simplex_ids.search(visible_simplex_id)
            except ValueError:
                visible_simplex_id = ridge[1]
                invisible_simplex_id = ridge[0]
#            print "vis, invis simpl ids", visible_simplex_id, invisible_simplex_id
            deleted_node_id_local = self.__ridge_simplex_node_id_local(ridge, visible_simplex_id)
            simplex = list(self.__simplices[visible_simplex_id])
            simplex[deleted_node_id_local] = inserted_node_id
            self.__simplices += [simplex]

            adjacency = [- (self.dimension + 2)] * (self.dimension + 1)
            adjacency[deleted_node_id_local] = invisible_simplex_id
            self.__adjacency += [adjacency]
            if invisible_simplex_id >= 0:
                node_id_local = self.__ridge_simplex_node_id_local(ridge, invisible_simplex_id)
                invisible_adjacency_update += [(invisible_simplex_id, node_id_local, inserted_simplex_id)]

            self.__children += [[]]

#        print "adjacency after pyramid is built"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120
                
#       build adjacency inside the hyperpyramid
        for ridge_1 in ridges:
#            print "ridge_1", ridge_1
            ridge_1_nodes = self.__ridge_nodes(ridge_1)
            simplex_id1 = ridge_indices[ridge_1]
            for ridge_2 in peaks[ridge_1]:
#                print "ridge_2", ridge_2
                ridge_2_nodes = self.__ridge_nodes(ridge_2)
                simplex_id2 = ridge_indices[ridge_2]

#                print "ridge_1_nodes", ridge_1_nodes
#                print "ridge_2_nodes", ridge_2_nodes

                node_id1 = ridge_1_nodes.difference(ridge_2_nodes); node_id1 = node_id1.pop()
#                node_2 = ridge_2_nodes.difference(ridge_1_nodes); node_2 = node_2.pop()

                node_id1_local = self.__simplices[simplex_id1].index(node_id1)
#                node_id2_local = self.__simplices[simplex_id2].index(node_2)

                self.__adjacency[simplex_id1][node_id1_local] = simplex_id2
#                self.__adjacency[simplex_id2][node_id2_local] = simplex_id1


#       build adjacency outside of the hyperpyramid
        for update in invisible_adjacency_update:
            invisible_simplex_id, node_id_local, inserted_simplex_id = update
            self.__adjacency[invisible_simplex_id][node_id_local] = inserted_simplex_id

                
#        print "adjacency after rewiering the pyramid"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120

#       clear adjacency lists and create children list
        common_children = []
        for ridge in ridges:
            simplex_id = ridge_indices[ridge]
            common_children += [simplex_id]
        for simplex_id in visible_simplex_ids:
            self.__adjacency[simplex_id] = [- (self.dimension + 2)] * (self.dimension + 1)
            self.__children[simplex_id] = common_children

#        print "adjacency after cleanup"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120

#       compute auxilary data
        for ridge in ridges:
            simplex_id = ridge_indices[ridge]
#           compute neighbors
            for node_id in self.__simplices[simplex_id]:
                self.__neighbors[node_id] = simplex_id
#           compute simplex hyperplane and barycentric coordinates
            X = []
            A = []
            b = []
            for node_id in self.__simplices[simplex_id]:
                X += [self.__nodes[node_id] + [1.0]]
                node_lift = self.__lf(self.__nodes[node_id])
                node = self.__nodes[node_id] + [node_lift, 1.0]
                A += [node]
                b += [0.0]
            A += [[0.0]*self.dimension + [self.__lfmax, 1.0]]
            b += [-1.0]
            n = solve(A, b)
#            if len(n) != 1:
#                raise MeshError('Points are not in general position')
#            n = n[0]
            self.__simplex_plane += [n]
            self.__barycentric += [np.linalg.inv(X).T]
            quality = self.__compute_quality(simplex_id)
            self.__quality += [quality]
            self.__suggested_refinement.push(quality[5], simplex_id)

        for simplex_id in visible_simplex_ids:
            try:
                self.__suggested_refinement.remove(simplex_id)
            except ValueError:
                pass

##
##  Suggested refinement node
##

    def suggested_node(self):
        try:
            key, simplex_id = self.__suggested_refinement.pop()
        except IndexError:
            return None, []
        quality = self.__quality[simplex_id]
        alpha = quality[6]
        x = [0.0] * self.dimension
        for node_id_local in xrange(self.dimension+1):
            node_id = self.__simplices[simplex_id][node_id_local]
            node_alpha = [xi * alpha[node_id_local] for xi in self.__nodes[node_id]]
            x = vector_sum(x, node_alpha)
        return key, x

##
##  Precompute neighbours of a vertex.
##

    def compute_neighbors(self):
        raise MeshError('compute_neighbors is not implemented for Delaunay triangulations')
        

##
##  Precompute simplicial barycentric coordinates.
##

    def precompute_barycentric(self):
        raise MeshError('precompute_barycentric is not implemented for Delaunay triangulations')

##
##  Precompute mesh quality.
##

    def precompute_quality(self):
        raise MeshError('precompute_quality is not implemented for Delaunay triangulations')

    def quality(self, mesh_id, simplex_id):
        raise MeshError('quality is not implemented for Delaunay triangulations')

##
##  Find simplex
##

    def find_simplex(self, x, simplex_id=None):
        x = np.array(x + [1.0])
        simplex_id = 0
        while self.__children[simplex_id]:
            for child_simplex_id in self.__children[simplex_id]:
                alpha = list(np.dot(self.__barycentric[child_simplex_id], x))                
                if all([a >= -1.e-8 for a in alpha]):
                    simplex_id = child_simplex_id
        return [simplex_id]

##
##  Compute the star of a vertex
##

    def star(self, node_id):
        return [(simplex_id, ) for simplex_id in self.get_neighbors(node_id)[0]]

##
##  Compute the geometry of a simplex
##

    def geometry(self, simplex_id):
        return [self.get_coordinates(node_id) for node_id in self.node_ids(simplex_id)]

##
##  Compute node indecies of a simplex
##

    def node_ids(self, simplex_id):
        return [(node_id, ) for node_id in self.get_simplex(simplex_id)[0]]

##
##  Compute projected node indecies of a projected simplex onto mesh_id
##

    def node_ids_projected(self, mesh_id, simplex_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return [node_id[0] for node_id in self.node_ids([simplex_id])]

##
##  Compute node indecies of a simplex so that node_id is at the origin of local coordinates
##

    def node_ids_local(self, simplex_id, node_id):
        return [(node_id_local,) for node_id_local in self.get_simplex_local(simplex_id, node_id)[0]]
