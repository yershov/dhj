from numpy import array
from bisect import *
from utils import *
import itertools
from vtk_writer import write_vtu
from math import isinf, isnan

class ResultsError(Exception):
    def __init__(self, value=""):
        self.value = value
    def __str__(self):
        return repr(self.value)


class Results:

    def __init__(self, mesh, sparse=True, sat=float('inf')):
        self.mesh = mesh
        self.sparse = sparse
        self.saturation = sat
        self.node_values_names = None
        self.cell_values_names = None
        self.node_values_mask = []
        self.node_vectors_mask = []
        self.cell_values_names = None
        self.cell_values_mask = []
        self.cell_vectors_mask = []

        self.reinitialize()

    def reinitialize(self):
        if self.sparse:
            self.node_values = []
            self.cell_values = []
        else:
            node_number = self.mesh.node_number()
            cell_number = self.mesh.simplex_number()
            self.node_values = [self.saturation] * node_number
            self.cell_values = [0.0] * node_number
        

    def addnode(self, node_id, values):
        if self.sparse:
            i = bisect_left(self.node_values, [node_id, 0])
            if i >= len(self.node_values) or self.node_values[i][0] != node_id:
#                insort_left(self.node_values, [node_id, values])
#                self.node_values = self.node_values[:i] + [[node_id, values]] + self.node_values[i:]
                self.node_values.insert(i, [node_id, values])
            else:
                self.node_values[i] = [node_id, values]
        else:
            i = 0
            for mesh_id in xrange(self.mesh.cartesian_number()):
                if mesh_id == 0:
                    i = node_id[-1]
                else:
                    i = node_id[-1-mesh_id] + self.mesh.node_number(mesh_id) * i
            self.node_values[i] = values

    def removenode(self, node_id):
        if self.sparse:
            i = bisect_left(self.node_values, [node_id, 0])
            if i >= len(self.node_values) or self.node_values[i][0] != node_id:
                raise KeyError('No such node id: ' + str(node_id))
            else:
                del self.node_values[i]

    def getnode(self, node_id):
        if self.sparse:
            i = bisect_left(self.node_values, [node_id, 0])
            if i >= len(self.node_values) or self.node_values[i][0] != node_id:
                raise KeyError('No such node id: ' + str(node_id))
            else:
                return self.node_values[i][1]
        else:
            i = 0
            for mesh_id in xrange(self.mesh.cartesian_number()):
                if mesh_id == 0:
                    i = node_id[-1]
                else:
                    i = node_id[-1-mesh_id] + self.mesh.node_number(mesh_id) * i
            return self.node_values[i]

    def addcell(self, cell_id, values):
        if self.sparse:
            i = bisect_left(self.cell_values, [cell_id, 0])
            if i >= len(self.cell_values) or self.cell_values[i][0] != cell_id:
#                insort_left(self.cell_values, [cell_id, values])
#                self.cell_values = self.cell_values[:i] + [[cell_id, values]] + self.cell_values[i:]
                self.cell_values.insert(i, [cell_id, values])
            else:
                self.cell_values[i] = [cell_id, values]
        else:
            i = 0
            for mesh_id in xrange(self.mesh.cartesian_number()):
                if mesh_id == 0:
                    i = cell_id[-1]
                else:
                    i = cell_id[-1-mesh_id] + self.mesh.simplex_number(mesh_id) * i
            self.cell_values[i] = [values]

    def removecell(self, cell_id):
        if self.sparse:
            i = bisect_left(self.cell_values, [cell_id, 0])
            if i >= len(self.cell_values) or self.cell_values[i][0] != cell_id:
                return KeyError('No such simplex id: ' + str(cell_id))
            else:
                del self.cell_values[i]

    def getcell(self, cell_id):
        if self.sparse:
            i = bisect_left(self.cell_values, [cell_id, 0])
            if i >= len(self.cell_values) or self.cell_values[i][0] != cell_id:
                return KeyError('No such simplex id: ' + str(cell_id))
            else:
                return self.cell_values[i][1]
        else:
            i = 0
            for mesh_id in xrange(self.mesh.cartesian_number()):
                if mesh_id == 0:
                    i = cell_id[-1]
                else:
                    i = cell_id[-1-mesh_id] + self.mesh.simplex_number(mesh_id) * i
            return self.cell_values[i]
        
    def value(self, x, simplex_id=None):
        simplex_id = self.mesh.find_simplex(x, simplex_id)
        
        

    def grad(self):
        if self.mesh.cartesian_number() > 1:
            raise MeshError('Gradient is supported for cartesian dimension 1 only')

        for computed_nodes in self.node_values:
            node_id = computed_nodes[0][0]

            neighbours = self.mesh.neighbors[0][node_id]

            for nbr in neighbours:
                N = self.getcell([nbr])
                if N == self.saturation:
                    self.addcell([nbr], 1)
                else:
                    self.addcell([nbr], N+1)
            
                    
            n_required = self.mesh.topological_dimension[0] + 1

            for computed_cells in self.cell_values:
                if computed_cells[1] < n_required:
                    self.removeCell(computed_cells[0])
                else:
                    simplex_id = computed_cells[0][0]
                    node_ids = self.mesh.simplices[0][simplex_id]
                    gradV = np.array([self.getnode(node_id)[0] for node_id in node_ids])
                    A = self.mesh.barycentric[0][simplex_id]
                    gradV = gradV * A
                    gradv = list(gradV)
                    gradV = gradV[:-1]
                    self.addcell(computed_cells[0], gradV)


    def export_vtk(self, filename):
        
        geometric_dimension = self.mesh.geometric_dimension()
        topological_dimension = self.mesh.topological_dimension()
        
        if geometric_dimension > 3:
            raise ResultsError('Cannot export mesh of geometric dimension higher than 3.')

        if topological_dimension > 3:
            raise ResultsError('Cannot export mesh of topological dimension higher than 3.')

##      consider all possible topological cases
##      case ==  3: 1
##      case ==  5: 2
##      case == 10: 3
##      case ==  9: 1 1
##      case == 13: 2 1 (subcase = 1) or 1 2 (subcase = 2)
##      case == 12: 1 1 1
        
        case = 0; subcase = 0

        if self.mesh.cartesian_number() == 1:
            if topological_dimension == 1:
                case = 3
            if topological_dimension == 2:
                case = 5
            if topological_dimension == 3:
                case = 10
        elif self.mesh.cartesian_number() == 2:
            if topological_dimension == 2:
                case = 9
            else:
                case = 13
                subcase = self.mesh.topological_dimensions()[1]
        else:
            case = 12

#        for i in range(len(self.node_values)):
#            node = self.node_values[i]
#            node[1] += [i]

        verts = [];
        pdata = [];

        node_id_min = None
        V_min = float('inf')

        node_counter = 0

        for i in xrange(len(self.node_values)):
            node = self.node_values[i]
            node_id = node[0]
            
            if isinf(node[1][0]) or isnan(node[1][0]):
                node[1] += [-1]
                continue

            node[1] += [node_counter]; node_counter += 1
            
            verts += [self.mesh.get_coordinates(node_id)]
            if geometric_dimension == 1:
                verts[-1] += [0.0] * 2
            elif geometric_dimension == 2:
                verts[-1] += [0.0]
            
            pdata += [[node[1][i] for i in self.node_values_mask]]
            
            if node[1][0] < V_min:
                V_min = node[1][0]
                node_id_min = node_id

        if node_id_min == None:
            return

        cells = []
        simplex_queue = list(self.mesh.star(node_id_min))
        computed_simplices = SortedList(simplex_queue)

        while simplex_queue:
#            print simplex_queue
            simplex_id = simplex_queue.pop(0)

            simplex = list(self.mesh.node_ids(simplex_id))
            if case == 12:
                simplex = simplex[0:2] + [simplex[3], simplex[2]] + \
                          simplex[4:6] + [simplex[7], simplex[6]]
            if case == 13 and subcase == 1:
                simplex = simplex[0::2] + simplex[1::2]
            if case == 9:
                simplex = simplex[0:2] + [simplex[3], simplex[2]]

            try:
                cell = [self.getnode(node_id)[-1] for node_id in simplex]
            except KeyError:
                continue
            else:
                if -1 in cell:
                    continue
                else:
                    cells += [cell]


            for node_id in simplex:
                    for simplex2 in self.mesh.star(node_id):
                        try:
                            computed_simplices.search(simplex2)
                        except ValueError:
                            simplex_queue.append(simplex2)
                            computed_simplices.insert(simplex2)

        if cells == []:
            return

        cells = {case:array(cells)}
        
        write_vtu(Verts=array(verts), 
                  Cells=cells,
                  pdata=array(pdata),
                  pname=[self.node_values_names[i] for i in self.node_values_mask],
                  fname=filename)
        
