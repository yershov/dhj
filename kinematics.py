from math import sqrt, sin, cos, pi
from tolerance import eps
from vtk_writer import write_vtu
from numpy import array

inf = float('inf')

def line_intersect(x11, y11, x12, y12, x21, y21, x22, y22):
#  Solve system:
#  [ x11 ]          [ x12 ]                [ x21 ]          [ x22 ]
#  [     ] alpha1 + [     ] (1 - alpha1) = [     ] alpha2 + [     ] (1 - alpha2)
#  [ y11 ]          [ y12 ]                [ y21 ]          [ y22 ]
#
# or similarly
#
#  [ x11 - x12   x22 - x21 ]  [ alpha1 ]   [ x22 - x12 ]
#  [                       ]  [        ] = [           ] 
#  [ y11 - y12   y22 - y21 ]  [ alpha2 ]   [ y22 - y12 ]

#    print x11, y11
#    print x12, y12
#    print x21, y21
#    print x22, y22

    a11 = x11 - x12; a12 = x22 - x21
    a21 = y11 - y12; a22 = y22 - y21
    b1 = x22 - x12; b2 = y22 - y12

#    print a11, a12
#    print a21, a22
#    print b1, b2
    
    det = a11 * a22 - a21 * a12
    
    if abs(det) < eps:
        return [inf, inf]
    
    alpha1 = (  a22 * b1 - a12 * b2) / det
    alpha2 = (- a21 * b1 + a11 * b2) / det

    return [alpha1, alpha2]
   

class Kinematics:

    def __init__(self, config):

        self.link_length = map(float, config.get('Arm', "Link Length").split(","))
        self.link_theta_min = map(float, config.get('Arm', "Link Theta Min").split(","))
        self.link_theta_max = map(float, config.get('Arm', "Link Theta Max").split(","))
        self.anchor = map(float, config.get('Arm', "Anchor").split(","))

        self.n_link = len(self.link_length)
        
        print "Kinematic chain of length ", self.n_link

        self.obs = map(lambda coord: map(lambda str_val: float(str_val.strip()) , coord.split(",")), config.get('Obstacles', "Coordinates").split(";"))
        
        self.n_obs = len(self.obs)

        self.obs_filename = config.get('IO', 'obsfile')

        print "Number of obstacles is ", self.n_obs


    def collision(self, theta):
        arm = self.arm(theta)
        # check arm to obstacle collision

        l_min = 1.e+5
        
        for i in xrange(self.n_link):
            x11 = arm[i][0]; y11 = arm[i][1];
            x12 = arm[i+1][0]; y12 = arm[i+1][1];

            for j in xrange(self.n_obs):
                x21 = self.obs[j][0]; y21 = self.obs[j][1];
                x22 = self.obs[j][2]; y22 = self.obs[j][3];

#                print "i = ", i, "j = ", j
                
                alpha1, alpha2 = line_intersect(x11, y11, x12, y12, x21, y21, x22, y22)

                if 0 <= alpha1 and alpha1 <= 1.0 and 0 <= alpha2 and alpha2 <= 1.0:
                    return 0.0

                l_min = min(l_min, \
                                min(abs(alpha1), abs(alpha1 - 1.0)) * \
                                sqrt((x11-x12)**2 + (y11 - y12)**2), \
                                min(abs(alpha2), abs(alpha2 - 1.0)) * \
                                sqrt((x21-x22)**2 + (y21 - y22)**2))

        # TODO: check arm to arm collision

        return l_min
            
            
        
    def arm(self, theta):
        arm = [list(self.anchor)]
        
        for i in xrange(self.n_link):
            alpha = arm[i][2] + theta[i] - 0.5
            l = self.link_length[i]
            arm += [[arm[i][0] + l * cos(2 * pi * alpha), \
                         arm[i][1] + l * sin(2 * pi * alpha), alpha]]
            
        return arm

    def export_env(self):
        
        obs_nodes = []
        obs_cells = []

        for j in xrange(self.n_obs):
            x1 = self.obs[j][0:2] + [0.0]
            x2 = self.obs[j][2:4] + [0.0]
            
            obs_nodes += [x1, x2]
            obs_cells += [[j*2, j*2 + 1]]

            write_vtu(Verts=array(obs_nodes), 
                      Cells={3:array(obs_cells)}, 
                      fname=self.obs_filename)
            
