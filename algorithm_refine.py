from utils import PriorityQueue, SortedList
from algorithm import AlgorithmError
#import itertools

#from convergence_tools import print_cycle_history


class BasicRefinement:

    def __init__(self, base, termination, *refinements):
        self.base = base
        self.refinements = refinements
        self.refinement_number = len(refinements)
        self.refinement_i = 0
        self.termination = termination
        self.__FirstStep = True

    def step(self):
        if not self.__FirstStep:
            print "Refining mesh with algorithm ", self.refinement_i + 1
            self.refinements[self.refinement_i].refine()
            self.base.boundary.reinitialize()
            self.base.termination.reinitialize()
            self.base.results.reinitialize()
            self.base.reinitialize()

            self.refinement_i += 1
            self.refinement_i = self.refinement_i % self.refinement_number

        print "Running base planner..."
        self.base.plan()
        self.__FirstStep = False
#        print self.update.V_star
        
        


    def plan(self):
        while not self.termination.terminate():
            self.step()
            self.termination.update_iteration()
            
