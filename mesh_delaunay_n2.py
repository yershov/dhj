import copy
import numpy as np
from utils import *
from math import sqrt
import itertools
from mesh import MeshError


def bounding_regular_simplex(d,c,r):
    x0 = (1.0 - sqrt(1.0 + d)) / d
    bc = (1.0 + x0) / (d + 1)
    r0 = - ( bc - 1.0 + bc * (d-1)) / sqrt(d)
    x = [map(lambda ci: (x0 - bc) * r / r0 + ci, c)]
    for i in xrange(d):
        e = [0.0]*d; e[i] = 1.0
        xi = map(lambda xi, ci: (xi - bc) * r / r0 + ci, e, c)
        x += [xi]
    return x
    

class DelaunayMesh:
    
    def __init__(self, dimension, bounding_nodes, lifting_function):
        self.dimension = dimension
        if len(bounding_nodes) != dimension+1:
            raise MeshError('Number of bounding nodes must be exactly ' + str(dimension+1) + ', but ' + str(len(bounding_nodes)) + ' are given')

        self.__lf = lifting_function

        self.__nodes = []
        self.__lfmax = 0.0
        X = []
        A = []
        b = []
        for node in bounding_nodes:
            self.__nodes += [node]
            node_lf = lifting_function(node)
            if node_lf > self.__lfmax:
                self.__lfmax = node_lf

            X += [node + [1.0]]
            a = node + [node_lf, 1.0]
            A += [a]
            b += [0.0]

        A += [[0.0]*self.dimension + [self.__lfmax, 1.0]]
        b += [-1.0]
        n = solve(A, b)
#        if len(n) != 1:
#            raise MeshError('Bounding nodes are not in general position')
#        n = n[0]

        for node in bounding_nodes:
            node_lf = lifting_function(node)
            a = node + [node_lf, 1.0]
        

        self.__simplex_plane = [n]
 
        self.__simplices = [range(self.dimension+1)]

        self.__children = [[]]
        self.__adjacency = [range(-1,-(self.dimension+2),-1)]

        self.__neighbors = [0]*(self.dimension+1)

        self.__barycentric = [np.linalg.inv(X).T]

        quality = self.__compute_quality(0)
        self.__quality = [quality]
        self.__suggested_refinement = PriorityQueue()
        self.__suggested_refinement.push(quality[5], 0)

        self.max_visible = 0
        self.max_ridges = 0
        self.max_peaks = 0

##
##  get cartesian number
##

    def cartesian_number(self):
        return 1

##
##  get node number
##

    def node_number(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return len(self.__nodes)

##
##  get simplex number
##

    def simplex_number(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return len(self.__simplices)

##
##  get geometric dimension
##

    def geometric_dimension(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.dimension

##
##  get topological dimension
##

    def topological_dimension(self, mesh_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.dimension

##
##  Extract simplex from a Delaunay mesh
##
    
    def get_simplex(self, simplex_id):
        simplex_id = simplex_id[0]
        return [list(self.__simplices[simplex_id])]

##
##  Extract simplex from a cartesian mesh such that node_id is at the origin of a local coorduinate system
##

    def get_simplex_local(self, simplex_id, node_id):
        node_id = node_id[0]
        simplex = self.get_simplex(simplex_id)[0]
        node_id_local = simplex.index(node_id)
        simplex = simplex[node_id_local:] + simplex[:node_id_local]
        return [simplex]

##
##  Extract node neighbors from a cartesian mesh
##

    def get_neighbors(self, node_id):
        node_id = node_id[0]
        simplex_ids = SortedList(duplicate=False)
        simplex_queue = [self.__neighbors[node_id]]
        while simplex_queue:
            simplex_id = simplex_queue.pop(0)
            simplex_ids.insert(simplex_id)
            for neighbor_id in self.__adjacency[simplex_id]:
                if neighbor_id < 0:
                    continue

                try:
                    simplex_ids.search(neighbor_id)
                except ValueError:
                    pass
                else:
                    continue

                try:
                    self.__simplices[neighbor_id].index(node_id)
                except ValueError:
                    continue
                else:
                    simplex_queue += [neighbor_id]
                        
        return [list(simplex_ids)]

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates(self, node_id):
        node_id = node_id[0]
        return list(self.__nodes[node_id])

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates_projected(self, mesh_id, node_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return self.get_coordinates([node_id])

##
##  Add n points
##
    def add_points(self, points):
        for point in points:
            self.add_point(point)


    def __compute_quality(self, simplex_id):
        nodes = self.__simplices[simplex_id]
        x = [self.__nodes[node_id] for node_id in nodes]
        lmin = float('inf')
        lmax = 0.0
        cosmin = float('inf')
        for x1 in x:
            for x2 in x:
                if x2 == x1:
                    continue
                x12 = vector_diff(x1,x2)
                l = norm(x12)
                if l > lmax:
                    lmax = l
                if l < lmin:
                    lmin = l
                for x3 in x:
                    if x3 == x1 or x3 == x2:
                        continue
                    x32 = vector_diff(x3,x2)
                    cos = dot(x12,x32) / l / norm(x32)
                    if cos < cosmin:
                        cosmin = cos
                    
        CM = [[0] + [1] * (self.dimension+1)]
        for i in xrange(self.dimension+1):
            CMrow = [1]
            for j in xrange(self.dimension+1):
                xij = vector_diff(x[i], x[j])
                lij = dot(xij, xij)
                CMrow += [lij]
            CM += [CMrow]

        CMinv = np.linalg.inv(CM)

        rmax = sqrt(-0.5 * CMinv[0][0])
        alpharmax = list(CMinv[0][1:self.dimension+2])

        return (lmin, lmin / lmax, cosmin, (), (), lmin / rmax, alpharmax)
            


    def __ridge_nodes(self, ridge):
        nodes = self.__simplices[ridge[1]]
        i = self.__adjacency[ridge[1]].index(ridge[0])
        nodes = set(nodes[:i] + nodes[i+1:])
        return nodes

    def __ridge_simplex_node_id_local(self, ridge, simplex_id):
        ridge_nodes = self.__ridge_nodes(ridge)
        simplex_nodes = self.__simplices[simplex_id]
        node = set(simplex_nodes).difference(ridge_nodes)
        node = node.pop()
        node_id_local = simplex_nodes.index(node)
        return node_id_local

##
##  Add a point
##
    def add_point(self, x):
#       find where point is inserted
        simplex_id = self.find_simplex(x)[0]
#        print "="*120
#        print x, " located in", simplex_id
#        print "adjacency before rewiering"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120
#
#       find visible simplices
#
        x_lifted = x + [self.__lf(x), 1.0]
        simplex_queue = SortedList([simplex_id],duplicate=False)
        visible_simplex_ids = SortedList(duplicate=False)
#        invisible_simplex_ids = SortedList(duplicate=False)
        while len(simplex_queue) > 0:
            simplex_id = simplex_queue.pop()
#            print "adjacency["+ str(simplex_id) + "]", self.__adjacency[simplex_id]
#           if simplex is invisible, then continue
            if dot(self.__simplex_plane[simplex_id], x_lifted) <= 0.0:
#               simplex is visible
#                print "invisible", simplex_id
#                invisible_simplex_ids.insert(simplex_id)
                continue
#           simplex is visible
#            print "visible", simplex_id
            visible_simplex_ids.insert(simplex_id)
#           add neighbors to the queue
            for i in xrange(self.dimension+1):
                neighbor_simplex_id = self.__adjacency[simplex_id][i]
                try:
                    visible_simplex_ids.search(neighbor_simplex_id)
#                    invisible_simplex_ids.search(neighbor_simplex_id)
                except ValueError:
                    if neighbor_simplex_id > 0:
                        simplex_queue.insert(neighbor_simplex_id)
#
#       at this point visible simplices are computed
#
        nvisible = len(visible_simplex_ids)
        if nvisible > self.max_visible:
            self.max_visible = nvisible
#        print "visible simplices", visible_simplex_ids

#       compute ridges (common faces of two simplices) of the horizon graph
        ridges = SortedList(duplicate=False)
        for simplex_id in visible_simplex_ids:
            for neighbor_simplex_id in self.__adjacency[simplex_id]:
                try:
                    visible_simplex_ids.search(neighbor_simplex_id)
                except ValueError:
                    ridge = [simplex_id, neighbor_simplex_id]; ridge.sort(); ridge = tuple(ridge)
                    ridges.insert(ridge)

        nridges = len(ridges)
        if nridges > self.max_ridges:
            self.max_ridges = nridges

#       compute peaks (common faces of two ridges) of the horizon graph
        peaks = {}
        npeaks = 0
        for ridge1 in ridges:
#            print "ridge1", ridge1
            ridge1_nodes = self.__ridge_nodes(ridge1)
#            print "ridge1_nodes", ridge1_nodes

            neighbor_ridges = []

            for ridge2 in ridges:
                if ridge2 == ridge1:
                    continue
                ridge2_nodes = self.__ridge_nodes(ridge2)
                peak = set.intersection(ridge1_nodes, ridge2_nodes)
                if len(peak) == self.dimension - 1:
#                    print "peak added between", ridge, "and", neighbor_ridge
                    neighbor_ridges += [ridge2]
                    npeaks += 1

            peaks[ridge1] = neighbor_ridges
#
#       at this point horizon graph is computed
#
        npeaks /= 2
        if npeaks > self.max_peaks:
            self.max_peaks = npeaks
        
#        print "ridges", ridges
#        print "peaks", peaks

#       add node
        inserted_node_id = len(self.__nodes)
        self.__nodes += [x]
        self.__neighbors += [len(self.__simplices)]
        
#       build hyperpyramid
        ridge_indices = {}
        invisible_adjacency_update = []
        for ridge in ridges:
#            print "ridge", ridge
            inserted_simplex_id = len(self.__simplices)
            ridge_indices[ridge] = inserted_simplex_id
            visible_simplex_id = ridge[0]
            invisible_simplex_id = ridge[1]
            try:
                visible_simplex_ids.search(visible_simplex_id)
            except ValueError:
                visible_simplex_id = ridge[1]
                invisible_simplex_id = ridge[0]
#            print "vis, invis simpl ids", visible_simplex_id, invisible_simplex_id
            deleted_node_id_local = self.__ridge_simplex_node_id_local(ridge, visible_simplex_id)
            simplex = list(self.__simplices[visible_simplex_id])
            simplex[deleted_node_id_local] = inserted_node_id
            self.__simplices += [simplex]

            adjacency = [- (self.dimension + 2)] * (self.dimension + 1)
            adjacency[deleted_node_id_local] = invisible_simplex_id
            self.__adjacency += [adjacency]
            if invisible_simplex_id >= 0:
                node_id_local = self.__ridge_simplex_node_id_local(ridge, invisible_simplex_id)
                invisible_adjacency_update += [(invisible_simplex_id, node_id_local, inserted_simplex_id)]

            self.__children += [[]]

#        print "adjacency after pyramid is built"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120
                
#       build adjacency inside the hyperpyramid
        for ridge_1 in ridges:
#            print "ridge_1", ridge_1
            ridge_1_nodes = self.__ridge_nodes(ridge_1)
            simplex_id1 = ridge_indices[ridge_1]
            for ridge_2 in peaks[ridge_1]:
#                print "ridge_2", ridge_2
                ridge_2_nodes = self.__ridge_nodes(ridge_2)
                simplex_id2 = ridge_indices[ridge_2]

#                print "ridge_1_nodes", ridge_1_nodes
#                print "ridge_2_nodes", ridge_2_nodes

                node_id1 = ridge_1_nodes.difference(ridge_2_nodes); node_id1 = node_id1.pop()
#                node_2 = ridge_2_nodes.difference(ridge_1_nodes); node_2 = node_2.pop()

                node_id1_local = self.__simplices[simplex_id1].index(node_id1)
#                node_id2_local = self.__simplices[simplex_id2].index(node_2)

                self.__adjacency[simplex_id1][node_id1_local] = simplex_id2
#                self.__adjacency[simplex_id2][node_id2_local] = simplex_id1


#       build adjacency outside of the hyperpyramid
        for update in invisible_adjacency_update:
            invisible_simplex_id, node_id_local, inserted_simplex_id = update
            self.__adjacency[invisible_simplex_id][node_id_local] = inserted_simplex_id

                
#        print "adjacency after rewiering the pyramid"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120

#       clear adjacency lists and create children list
        common_children = []
        for ridge in ridges:
            simplex_id = ridge_indices[ridge]
            common_children += [simplex_id]
        for simplex_id in visible_simplex_ids:
            self.__adjacency[simplex_id] = [- (self.dimension + 2)] * (self.dimension + 1)
            self.__children[simplex_id] = common_children

#        print "adjacency after cleanup"
#        print "-"*120
#        print [str(simplex_id_) + " : " + str(self.__adjacency[simplex_id_]) for simplex_id_ in range(self.simplex_number(0))]
#        print "-"*120

#       compute auxilary data
        for ridge in ridges:
            simplex_id = ridge_indices[ridge]
#           compute neighbors
            for node_id in self.__simplices[simplex_id]:
                self.__neighbors[node_id] = simplex_id
#           compute simplex hyperplane and barycentric coordinates
            X = []
            A = []
            b = []
            for node_id in self.__simplices[simplex_id]:
                X += [self.__nodes[node_id] + [1.0]]
                node_lift = self.__lf(self.__nodes[node_id])
                node = self.__nodes[node_id] + [node_lift, 1.0]
                A += [node]
                b += [0.0]
            A += [[0.0]*self.dimension + [self.__lfmax, 1.0]]
            b += [-1.0]
            n = solve(A, b)
#            if len(n) != 1:
#                raise MeshError('Points are not in general position')
#            n = n[0]
            self.__simplex_plane += [n]
            self.__barycentric += [np.linalg.inv(X).T]
            quality = self.__compute_quality(simplex_id)
            self.__quality += [quality]
            self.__suggested_refinement.push(quality[5], simplex_id)

        for simplex_id in visible_simplex_ids:
            try:
                self.__suggested_refinement.remove(simplex_id)
            except ValueError:
                pass

##
##  Suggested refinement node
##

    def suggested_node(self):
        try:
            key, simplex_id = self.__suggested_refinement.pop()
        except IndexError:
            return None, []
        quality = self.__quality[simplex_id]
        alpha = quality[6]
        x = [0.0] * self.dimension
        for node_id_local in xrange(self.dimension+1):
            node_id = self.__simplices[simplex_id][node_id_local]
            node_alpha = [xi * alpha[node_id_local] for xi in self.__nodes[node_id]]
            x = vector_sum(x, node_alpha)
        return key, x

##
##  Precompute neighbours of a vertex.
##

    def compute_neighbors(self):
        raise MeshError('compute_neighbors is not implemented for Delaunay triangulations')
        

##
##  Precompute simplicial barycentric coordinates.
##

    def precompute_barycentric(self):
        raise MeshError('precompute_barycentric is not implemented for Delaunay triangulations')

##
##  Precompute mesh quality.
##

    def precompute_quality(self):
        raise MeshError('precompute_quality is not implemented for Delaunay triangulations')

    def quality(self, mesh_id, simplex_id):
        raise MeshError('quality is not implemented for Delaunay triangulations')

##
##  Find simplex
##

    def find_simplex(self, x, simplex_id=None):
        x = np.array(x + [1.0])
        simplex_id = 0
        while self.__children[simplex_id]:
            for child_simplex_id in self.__children[simplex_id]:
                alpha = list(np.dot(self.__barycentric[child_simplex_id], x))                
                if all([a >= -1.e-8 for a in alpha]):
                    simplex_id = child_simplex_id
        return [simplex_id]

##
##  Compute the star of a vertex
##

    def star(self, node_id):
        return [(simplex_id, ) for simplex_id in self.get_neighbors(node_id)[0]]

##
##  Compute the geometry of a simplex
##

    def geometry(self, simplex_id):
        return [self.get_coordinates(node_id) for node_id in self.node_ids(simplex_id)]

##
##  Compute node indecies of a simplex
##

    def node_ids(self, simplex_id):
        return [(node_id, ) for node_id in self.get_simplex(simplex_id)[0]]

##
##  Compute projected node indecies of a projected simplex onto mesh_id
##

    def node_ids_projected(self, mesh_id, simplex_id):
        if mesh_id != 0:
            raise MeshError('Delaunay triangulation supports only one cartesian component')
        return [node_id[0] for node_id in self.node_ids([simplex_id])]

##
##  Compute node indecies of a simplex so that node_id is at the origin of local coordinates
##

    def node_ids_local(self, simplex_id, node_id):
        return [(node_id_local,) for node_id_local in self.get_simplex_local(simplex_id, node_id)[0]]
