import bisect
from utils import norm, vector_diff

class BoundaryError(Exception):
    def __init__(self, value=""):
        self.value = value
    def __str__(self):
        return repr(self.value)

class BoundaryMarker:

    def __init__(self, mesh, marker, g = (lambda x: 0.0)):
        self.mesh = mesh
        self.marker = marker
        self.g = g
        
    def dirichlet(self): 
        return [(node_id,  self.g(self.mesh.get_coordinates(node_id))) for node_id in self.marker.mark_nodes()]

    def isboundary(self, node_id):
        return self.marker.test_node(node_id)

class UnitAxisPeriodicBoundary:

    def __init__(self, mesh, axis, tolerance=1.e-5):
        geometric_dimension = mesh.geometric_dimension()
        boundary_node_ids = []

        for node_id in mesh.node_iterator():
            x = mesh.get_coordinates(node_id)
            connections = set()
            is_periodic = False
            for axes in axis:
                if abs(x[axes]) < tolerance or abs(x[axes] - 1.0) < tolerance:
#                    if not is_periodic:
#                        print 1, node_id, x
                    is_periodic = True
                    for remote_node_id, remote_connections in boundary_node_ids:
                        remote_x = mesh.get_coordinates(remote_node_id)
#                        print 2, remote_node_id, remote_x, remote_connections
                        if remote_x[axes] > x[axes]:
                            remote_x[axes] -= 1.0
                        else:
                            remote_x[axes] += 1.0

                        distance = norm(vector_diff(x, remote_x))
                        
#                        print 3, distance

                        if distance < tolerance:
                            remote_connections.add(node_id)
                            connections.add(remote_node_id)
                            
            if is_periodic:
                boundary_node_ids.append((node_id, connections))
#                print 4, boundary_node_ids

        for _ in xrange(len(axis) - 1):
            for node_id, connections in boundary_node_ids:
                for remote_node_id in list(connections):
                    index = bisect.bisect_left(boundary_node_ids, (remote_node_id, set()))
                    if boundary_node_ids[index][0] != remote_node_id:
                        raise BoundaryError('Something went horribly wrong with periodic boundary conditions')
                    remote_connections = boundary_node_ids[index][1]
                    for remote_node_id2 in remote_connections:
                        if remote_node_id2 == node_id:
                            continue
                        connections.add(remote_node_id2) 

        index = 0
        for node_id, connections in boundary_node_ids:
            boundary_node_ids[index] = (node_id, list(connections))
            index += 1

        self.boundary_node_ids = boundary_node_ids

    
    def isboundary(self, node_id):
        index = bisect.bisect_left(self.boundary_node_ids, (node_id, []))
        try:
            if self.boundary_node_ids[index][0] == node_id:
                return True
        except IndexError:
            pass

        return False

    def connections(self, node_id):
        index = bisect.bisect_left(self.boundary_node_ids, (node_id, []))
        if self.boundary_node_ids[index][0] != node_id:
            raise IndexError('Not periodic node_id='+str(node_id))
        return self.boundary_node_ids[index][1]
    
