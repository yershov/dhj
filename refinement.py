import utils
from math import sqrt

class RefinementError(Exception):
    def __init__(self, value=""):
        self.value = value
    def __str__(self):
        return repr(self.value)

class HeuristicRefinement:
    
    def __init__(self, mesh, results, heuristic, V_star = 0.0):
        self.mesh = mesh
        self.results = results
        self.heuristic = heuristic
        self.V_star = V_star

        if self.mesh.cartesian_number > 1:
            raise RefinementError('Heuristic Refinement supports simplicial meshes only')

        
    def refinement_nodes(self):
        V_index = self.results.node_values_names.index('V')

        self.node_ids = []

        for node_id, values in self.results.node_values:
            V = values[V_index]
            H = self.heuristic(self.mesh.get_coordinates(node_id))
            if V + H <= self.V_star:
                self.node_ids += [node_id]
    
    def refinement_edges(self):
        self.refine_edges = []

        edges = utils.PriorityQueue()
        simpleces = utils.SortedList(duplicate=False)
        
        for node_id_0 in self.node_ids:
            x0 = self.mesh.get_coordinates(node_id_0)
            simplex_ids = self.mesh.star(node_id_0)
            for simplex_id in simplex_ids:
                simpleces.insert(simplex_id)
                simplex = self.mesh.node_ids(simplex_id)
                for node_id_1 in simplex:
                    if node_id_1 == node_id_0:
                        continue
                    x1 = self.mesh.get_coordinates(node_id_1)

                    dist = sum(map(lambda a, b: (a - b)**2, x0, x1))
                    
                    obj = (node_id_0, node_id_1)
                    if node_id_0 > node_id_1:
                        obj = (node_id_1, node_id_0)

                    edges.push( - dist, obj)

        
        while not edges.is_empty():
            dist, edge = edges.pop()
            node_id_0, node_id_1 = edge
            
            simplex_ids_0 = self.mesh.star(node_id_0)
            simplex_ids_1 = self.mesh.star(node_id_1)
            simplex_ids = list(set.intersection(set(simplex_ids_0), set(simplex_ids_1)))
            
            is_interfering = False

            for simplex_id in simplex_ids:
                try:
                    simpleces.search(simplex_id)
                except ValueError:
                    is_interfering = True

            if not is_interfering:
                for simplex_id in simplex_ids:
                    simpleces.remove(simplex_id)
                self.refine_edges += [(node_id_0[0], node_id_1[0], 0.5)]
                
        self.refine_edges = [self.refine_edges]


    def refine(self):
        self.refinement_nodes()
        self.refinement_edges()
        self.mesh.refine_edges(self.refine_edges)
        self.mesh.precompute_barycentric()
