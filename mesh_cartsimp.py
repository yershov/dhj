import copy
import numpy as np
from utils import *
from math import sqrt
from mesh import MeshError
import itertools

def list2mesh(l):
    try:
        geometric_dimension = len(l[0])
    except TypeError:
        geometric_dimension = 1

    topological_dimension = 1
    if geometric_dimension == 1:
        nodes = [[node] for node in l]
    else:
        nodes = list(l)
    n = len(nodes)
    simplices = []
    for i in xrange(n):
        simplices.append([i, i+1])
    simplices.pop()
    
    return geometric_dimension, topological_dimension, nodes, simplices

def regular_interval(a, b, n):
    geometric_dimension = 1
    topological_dimension = 1
    nodes = []
    simplices = []
    for i in xrange(n):
        t = float(i) / (n - 1)
        nodes.append([a * (1.0 - t) + b * t])
        simplices.append([i, i+1])
    simplices.pop()
    
    return geometric_dimension, topological_dimension, nodes, simplices

def regular_interval_array(a, b, n):
    if len(a) != len(b):
        raise MeshError('Arrays of different sizes: len(a) = '+str(len(a))+' and len(b) = '+str(len(b)))
    geometric_dimension = len(a)
    topological_dimension = 1
    nodes = []
    simplices = []
    for i in xrange(n):
        t = float(i) / (n - 1)
        nodes.append(map(lambda element_a, element_b: element_a * (1.0 - t) + element_b * t, a, b))
        simplices.append([i, i+1])
    simplices.pop()
    
    return geometric_dimension, topological_dimension, nodes, simplices

def load_mesh(mshfile, topological_dimension):
    geometric_dimension = 0
    nodes = []
    simplices = []

    node_refs = {}
    node_count = 0

    readmode = 0
    print 'Reading %s'%mshfile.name
    for line in mshfile:
        line = line.strip()
        if line.startswith('$'):
            if  line == '$Nodes':
                readmode = 1
            elif line == '$Elements':
                readmode = 2
            else:
                readmode = 0
        elif readmode:
            columns = line.split()
            if readmode == 1 and len(columns) > 1:
                try:
                    node_refs[int(columns[0])] = node_count
                    node_count += 1
                    nodes.append(map(float, columns[1:]))
                except ValueError:
                    raise MeshError('Node format error at line '+line)
            elif readmode == 2 and len(columns) > 1:
                try:
                    columns = map(int, columns)
                except ValueError:
                    raise MeshError('Element format error at line '+line)
                else:
                    (id, type) = columns[0:2]
                    ntags = columns[2]
                    tags = columns[3:3+ntags]
                    n_ids = columns[3+ntags:]
                    if len(n_ids) == topological_dimension + 1:
                        simplices.append([node_refs[n_id] for n_id in n_ids])
    print '  %d Nodes'%len(nodes)
    print '  %d Simplices'%len(simplices)

    geometric_dimension = len(nodes[0])

    return geometric_dimension, topological_dimension, nodes, simplices


class CartSimpMesh:
    
    def __init__(self, search_cachesize=5, tolerance=1.e-8):
        self.__cartesian_number = 0
        self.__geometric_dimension = []
        self.__topological_dimension = []
        self.__nodes = []
        self.__node_number = []
        self.__simplices = []
        self.__simplex_number = []
        self.__search_cache = []
        self.__cache_size = search_cachesize
        self.__tol = tolerance
        self.has_periodic_boundary = False

##
##  Ads simplicial mesh as an extra cartesian product
##
    def add_simplicial_mesh(self, geometric_dimension, topological_dimension, nodes, simplices):
        self.__cartesian_number += 1
        self.__geometric_dimension.append(geometric_dimension)
        self.__topological_dimension.append(topological_dimension)
        self.__nodes.append(nodes)
        self.__node_number.append(len(nodes))
        self.__simplices.append(simplices)
        self.__simplex_number.append(len(simplices))

##
##  Precompute neighbours of a vertex.
##

    def compute_neighbors(self):
        self.__neighbors = []
        for mesh_id in xrange(self.__cartesian_number):
            nodes = self.__nodes[mesh_id]
            simplices = self.__simplices[mesh_id]
            n_simplex = self.__simplex_number[mesh_id]
            neighbors = [[] for node_id in xrange(self.__node_number[mesh_id])]

            for simplex_id in xrange(n_simplex):
                simplex = simplices[simplex_id]
                for node_id in simplex:
                    neighbors[node_id].append(simplex_id)

            self.__neighbors.append(neighbors)

##
##  Precompute simplicial barycentric coordinates.
##

    def precompute_barycentric(self):
        self.__barycentric = []
        for mesh_id in xrange(self.__cartesian_number):
            self.__barycentric.append([])
            for simplex in self.__simplices[mesh_id]:
                ## shift the origin into the simplicial plane
                ## or use biorthogonal basis projection method
                X = np.array([self.__nodes[mesh_id][node_id] + [1.0] for node_id in simplex]).T
                self.__barycentric[mesh_id].append(np.linalg.pinv(X))

##
##  Precompute mesh quality.
##

    def precompute_quality(self):
        self.__quality = []
        for mesh_id in xrange(self.__cartesian_number):
            self.__quality.append([])
            for simplex in self.__simplices[mesh_id]:
                lmin = float('inf')
                lmax = 0.0
                cosmin = float('inf')
                nodemin = None
                nodemax = None
                for node_id1 in simplex:
                    x1 = self.__nodes[mesh_id][node_id1]
                    for node_id2 in simplex:
                        if node_id1 == node_id2:
                            continue
                        x2 = self.__nodes[mesh_id][node_id2]
                        x12 = vector_diff(x1,x2)
                        l = norm(x12)
                        for node_id3 in simplex:
                            if node_id1 == node_id3 or node_id2 == node_id3:
                                continue
                            x3 = self.__nodes[mesh_id][node_id3]
                            x32 = vector_diff(x3,x2)
                            cos = dot(x12,x32) / l / norm(x32)
                            if cos < cosmin:
                                cosmin = cos
                        if node_id2 < node_id1:
                            continue

                        if l > lmax:
                            lmax = l
                            nodemax = (node_id1, node_id2)
                        if l < lmin:
                            lmin = l
                            nodemin = (node_id1, node_id2)    

                # CM = [[0] + [1] * (self.dimension+1)]
                # for i in range(self.dimension+1):
                #     CMrow = [1]
                #     for j in range(self.dimension+1):
                #         xij = vector_diff(x[i], x[j])
                #         lij = dot(xij, xij)
                #         CMrow += [lij]
                #     CM += [CMrow]

                # CMinv = np.linalg.inv(CM)

                # rmax = sqrt(-0.5 * CMinv[0][0])
                # alpharmax = list(CMinv[0][1:self.dimension+2])
                
                self.__quality[mesh_id].append((lmin, lmin / lmax, cosmin, nodemin, nodemax, 0.0, []))


##
## Mask based refinement
## for each element mask[i] = (node_id_1, node_id_2, alpha)
## refine neighbouring simplex by splitting the edge (node_id_1, node_id_2) into alpha and (1 - alpha)
## intervals
## this procedure refines along the long edge if mask = None
##

    def refine_edges(self, mask):

        if len(mask) != self.__cartesian_number:
            raise MeshError('Mask is '+str(len(mask))+' long, but the cartisian number is '+str(self.__cartesian_number))
        
        for mesh_id in xrange(self.__cartesian_number):
            while mask[mesh_id]:
                node_id_1, node_id_2, alpha = mask[mesh_id].pop()
                
                node_id_inserted = self.__node_number[mesh_id]
                self.__nodes[mesh_id].append([alpha * self.__nodes[mesh_id][node_id_1][i] +
                                              (1.0 - alpha) * self.__nodes[mesh_id][node_id_2][i] 
                                              for i in xrange(self.__geometric_dimension[mesh_id])])
                self.__node_number[mesh_id] += 1
                self.__neighbors[mesh_id].append([])

                # compute simplices to rewire
                node_id_1_nbr = set(self.__neighbors[mesh_id][node_id_1])
                node_id_2_nbr = set(self.__neighbors[mesh_id][node_id_2])
                rewire_simplices = list(set.intersection(node_id_1_nbr, node_id_2_nbr))
                
                # rewire simplices
                for simplex_id in rewire_simplices:
                    # compute the splitted edge in the simplex
                    node_1 = self.__simplices[mesh_id][simplex_id].index(node_id_1)
                    node_2 = self.__simplices[mesh_id][simplex_id].index(node_id_2)

                    simplex_id_inserted = self.__simplex_number[mesh_id]

                    # modify the neighbour map
                    for node_id in self.__simplices[mesh_id][simplex_id]:
                        if node_id != node_id_1 and node_id != node_id_2:
                            self.__neighbors[mesh_id][node_id].append(simplex_id_inserted)
                    
                    neighbour_index = self.__neighbors[mesh_id][node_id_2].index(simplex_id)
                    self.__neighbors[mesh_id][node_id_2][neighbour_index] = simplex_id_inserted

                    self.__neighbors[mesh_id][node_id_inserted].append(simplex_id, simplex_id_inserted)

                    
                    # insert a new simplex
                    simplex_inserted = list(self.__simplices[mesh_id][simplex_id])
                    simplex_inserted[node_1] = node_id_inserted
                    self.__simplices[mesh_id].append(simplex_inserted)
                    self.__simplex_number[mesh_id] += 1
                    
                    # modify the existing simplex
                    self.__simplices[mesh_id][simplex_id][node_2] = node_id_inserted

                    
                
##        
## Triangulate cartesian mesh. The result is a simplicial mesh.
##

    def triangulate(self):
        if self.__cartesian_number > 2:
            raise MeshError('This class supports triangulations of two cartesian meshes only')

        if self.__cartesian_number < 2:
            return copy.deepcopy(self)

        if self.__topological_dimension[0] != 1 and self.__topological_dimension[1] != 1:
            raise MeshError('This class supports triangulations of two cartesian meshes, one of which is topological dimension 1 strictly')        


        triangulation = CartSimpMesh()

        base = 0
        height = 1

        if self.__topological_dimension[1] != 1:
            base = 1
            height = 0
            
        base_geometric_dimension = self.__geometric_dimension[base]
        base_topological_dimension = self.__topological_dimension[base]
        base_nodes = self.__nodes[base]
        base_node_number = self.__node_number[base]
        base_simplices = self.__simplices[base]
        base_simplex_number = self.__simplex_number[base]

        height_geometric_dimension = self.__geometric_dimension[height]
        height_topological_dimension = self.__topological_dimension[height]
        height_nodes = self.__nodes[height]
        height_node_number = self.__node_number[height]
        height_simplices = self.__simplices[height]
        height_simplex_number = self.__simplex_number[height]

        triangulation.__cartesian_number = 1
        triangulation.__geometric_dimension = [base_geometric_dimension + height_geometric_dimension]
        triangulation.__topological_dimension = [base_topological_dimension + height_topological_dimension]

        triangulation.__nodes = [[]]

        # Vertex coordinates are cartesian product of the base and height coordinates
        for height_node_id in xrange(height_node_number):
            for base_node_id in xrange(base_node_number):
                triangulation.__nodes[0].append(base_nodes[base_node_id] + height_nodes[height_node_id] if base < height else height_nodes[height_node_id] + base_nodes[base_node_id])
        triangulation.__node_number = [base_node_number * height_node_number]
        if triangulation.__node_number[0] != len(triangulation.__nodes[0]):
            print "Node number achtung!", triangulation.__node_number[0], len(triangulation.__nodes[0])
        triangulation.__simplices = [[]]
        
        # Simplices are formed by adding a height node to each consequitive vertex
        # If the base mesh is consistent, then the resulting triangulation is consistent
        for height_simplex_id in xrange(height_simplex_number):
            height_nodes = height_simplices[height_simplex_id]
            for base_simplex_id in xrange(base_simplex_number):
                base_nodes = [base_node_id + base_node_number * height_nodes[0] for base_node_id in  base_simplices[base_simplex_id]]
                index = range(base_topological_dimension+1)
                base_index = zip(base_nodes, index)
                base_index.sort()
                index = [i for _,i in base_index]
                for i in xrange(base_topological_dimension+1):
                    triangulation.__simplices[0].append(base_nodes + [base_nodes[index[i]] + base_node_number])
                    base_nodes[index[i]] += base_node_number
        triangulation.__simplex_number = [base_simplex_number * height_simplex_number * (base_topological_dimension + 1)]
        if triangulation.__simplex_number[0] != len(triangulation.__simplices[0]):
            print "Simplex number achtung!", triangulation.__simplex_number[0], len(triangulation.__simplices[0])

        return triangulation

##
##  Set axis aligned periodic boundary.
##

    def set_axis_periodic(self, axis):
        geometric_dimension = self.geometric_dimension()
        periodic_node_ids = []

        for node_id in self.node_iterator():
            x = self.get_coordinates(node_id)
            connections = set()
            is_periodic = False
            for axes in axis:
                if abs(x[axes]) < self.__tol or abs(x[axes] - 1.0) < self.__tol:
#                    if not is_periodic:
#                        print 1, node_id, x
                    is_periodic = True
                    for remote_node_id, remote_connections in periodic_node_ids:
                        remote_x = self.get_coordinates(remote_node_id)
#                        print 2, remote_node_id, remote_x, remote_connections
                        if remote_x[axes] > x[axes]:
                            remote_x[axes] -= 1.0
                        else:
                            remote_x[axes] += 1.0

                        distance = norm(vector_diff(x, remote_x))
                        
#                        print 3, distance

                        if distance < self.__tol:
                            remote_connections.add(node_id)
                            connections.add(remote_node_id)
                            
            if is_periodic:
                periodic_node_ids.append((node_id, connections))
#                print 4, periodic_node_ids

        for _ in xrange(len(axis) - 1):
            for node_id, connections in periodic_node_ids:
                for remote_node_id in list(connections):
                    index = bisect.bisect_left(periodic_node_ids, (remote_node_id, set()))
                    if periodic_node_ids[index][0] != remote_node_id:
                        raise MeshError('Something went terribly wrong with periodic boundary conditions')
                    remote_connections = periodic_node_ids[index][1]
                    for remote_node_id2 in remote_connections:
                        if remote_node_id2 == node_id:
                            continue
                        connections.add(remote_node_id2) 

        index = 0
        for node_id, connections in periodic_node_ids:
            if len(connections) == 0:
                raise MeshError('Cannot compute periodic nodes')
            periodic_node_ids[index] = (node_id, list(connections))
            index += 1

        self.__periodic_node_ids = periodic_node_ids
        self.has_periodic_boundary = True



##  ===============================================
##  ============== I N T E R F A C E ==============
##  ===============================================

##
##  get cartesian number
##

    def cartesian_number(self):
        return self.__cartesian_number

##
##  get geometric dimension
##

    def geometric_dimension(self):
        return sum(self.__geometric_dimension)

##
##  get geometric dimensions
##

    def geometric_dimensions(self):
        return list(self.__geometric_dimension)

##
##  get topological dimension
##

    def topological_dimension(self):
        return sum(self.__topological_dimension)

##
##  get topological dimensions
##

    def topological_dimensions(self):
        return list(self.__topological_dimension)

##
##  get node number
##

    def node_number(self):
        return int(prod(self.__node_number))

##
##  get simplex number
##

    def simplex_number(self):
        return int(prod(self.__simplex_number))

##
##  get node iterator
##

    def node_iterator(self):
        return itertools.product(*[xrange(self.__node_number[mesh_id]) for mesh_id in xrange(self.__cartesian_number)])

##
##  get simplex iterator
##

    def simplex_iterator(self):
        return itertools.product(*[xrange(self.__simplex_number[mesh_id]) for mesh_id in xrange(self.__cartesian_number)])

##
##  Compute node indecies of a simplex
##

    def node_ids(self, simplex_id):
        return itertools.product( *[ self.__simplices[mesh_id][simplex_id[mesh_id]] for mesh_id in xrange(self.__cartesian_number) ] )

##
##  Compute node indecies of a simplex so that node_id is at the origin of local coordinates
##

    def node_ids_local(self, simplex_id, node_id):
        simplex = [ self.__simplices[mesh_id][simplex_id[mesh_id]] for mesh_id in xrange(self.__cartesian_number) ]
        for mesh_id in xrange(self.__cartesian_number):
            node_id_local = simplex[mesh_id].index(node_id[mesh_id])
            simplex[mesh_id] = simplex[mesh_id][node_id_local:] + simplex[mesh_id][:node_id_local]
        return itertools.product(*simplex)

##
##  Compute the star of a vertex
##

    def star(self, node_id):
        return itertools.product( *[self.__neighbors[mesh_id][node_id[mesh_id]] for mesh_id in xrange(self.__cartesian_number) ] )

##
##  Extract node coordinates from a cartesian mesh
##

    def get_coordinates(self, node_id):
        return sum([self.__nodes[mesh_id][node_id[mesh_id]] for mesh_id in xrange(self.__cartesian_number)], [])

##
##  Compute the geometry of a simplex
##

    def geometry(self, simplex_id):
        return [self.get_coordinates(node_id) for node_id in self.node_ids(simplex_id)]

##
##  quality parameters of a simplex (TODO)
##

    def get_simplex_quality(self, simplex_id):
        return self.__quality[0][simplex_id[0]]

##
##  Find simplex
##

    def find_simplex(self, x, simplex_id=None):
        result_simplex_id = []
#        print "input coordinates", x
        if simplex_id == None and len(self.__search_cache) == 0:
            simplex_id = [0] * self.__cartesian_number

        index_counter = 0
        for mesh_id in xrange(self.__cartesian_number):
            geometric_dimension = self.__geometric_dimension[mesh_id]
            x_projected = np.array(x[index_counter:index_counter+geometric_dimension] + [1.0])
            index_counter += geometric_dimension

            simplex_queue = [cached_simplex_id[mesh_id] for cached_simplex_id in  self.__search_cache]
            if simplex_id != None:
                simplex_queue.insert(0, simplex_id[mesh_id])
            checked_simplices = SortedList(simplex_queue)
            simplex_found = False
 
            while not simplex_found and simplex_queue:
                simplex_id1 = simplex_queue.pop(0)
                
                alpha = list(np.dot(self.__barycentric[mesh_id][simplex_id1], x_projected))
                x_simplex = np.array([0.0] * (geometric_dimension + 1))
                for i in xrange(self.__topological_dimension[mesh_id] + 1):
                    node_id = self.__simplices[mesh_id][simplex_id1][i]
                    x_simplex += np.array(self.__nodes[mesh_id][node_id] + [1.0]) * alpha[i]
#                    print "test coordinate", i, self.__nodes[mesh_id][node_id]
                    

#                print "barycentric coordinates", alpha

#                print "closest point", x_simplex

                if all([a >= -1.e-8 for a in alpha]) and np.linalg.norm(x_simplex - x_projected) < 1.e-8:
#                    print "simplex found"
                    simplex_found = True

#                print "simplex not found"

                for node_id in self.__simplices[mesh_id][simplex_id1]:
                    for simplex_id2 in self.__neighbors[mesh_id][node_id]:
                        try:
                            checked_simplices.search(simplex_id2)
                        except ValueError:
                            simplex_queue.append(simplex_id2)
                            checked_simplices.insert(simplex_id2)

            if simplex_found:
                result_simplex_id.append(simplex_id1)
            else:
                raise ValueError("Simplex not found: the point is outside of the domain.")

        if not result_simplex_id in self.__search_cache: 
            self.__search_cache.insert(0, result_simplex_id)
        if len(self.__search_cache) > self.__cache_size:
            self.__search_cache.pop()

        return result_simplex_id


##
##  Compute barycentric coordinates
##  TODO: here one must be careful to check whethere itertools.product()
##  results in the same node enumerations as node_ids() and geometry()
##  at the first glance, it looks like it
##

    def compute_barycentric(self, x, simplex_id):
        index_counter = 0
        alpha = []
        for mesh_id in xrange(self.__cartesian_number):
            geometric_dimension = self.__geometric_dimension[mesh_id]
            x_projected = np.array(x[index_counter:index_counter+geometric_dimension] + [1.0])
            index_counter += geometric_dimension

            alpha.append(list(np.dot(self.__barycentric[mesh_id][simplex_id[mesh_id]], x_projected)))

        alpha = itertools.product(*alpha)
        alpha = [a for a in alpha]
        alpha = [prod(a) for a in alpha]

        return alpha

##
##  Compute projected node indecies of a projected simplex onto mesh_id
##
#    def node_ids_projected(self, mesh_id, simplex_id):
#        return self.__simplices[mesh_id][simplex_id]
##
##  Extract node coordinates from a cartesian mesh
##
#    def get_coordinates_projected(self, mesh_id, node_id):
#        return self.__nodes[mesh_id][node_id]

##
##  
##

    def is_periodic(self, node_id):
        index = bisect.bisect_left(self.__periodic_node_ids, (node_id, []))
        try:
            if self.__periodic_node_ids[index][0] == node_id:
                return True
        except IndexError:
            pass

        return False

    def periodic_connections(self, node_id):
        index = bisect.bisect_left(self.__periodic_node_ids, (node_id, []))
        if self.__periodic_node_ids[index][0] != node_id:
            raise IndexError('Not a periodic node_id (node_id = '+str(node_id)+'')
        return self.__periodic_node_ids[index][1]
