import random
from utils import *
from mesh import MeshError

class AbstractSampler:
    def sample():
        raise NotImplementedError('sample is not implemented in AbstractSampler')
        

class UniformCube(AbstractSampler):
    
    def __init__(self, mesh, bounding_box):
        self.__mesh = mesh
        self.__bounding_box = bounding_box
        self.__dimension = len(bounding_box)

    def sample(self):
        vertex = [random.uniform(self.__bounding_box[0][i], self.__bounding_box[1][i]) for i in xrange(self.__dimension)]
        self.__mesh.add_vertex(vertex)


#
#  todo: rewrite with mesh
#
class UniformSimplex(AbstractSampler):
    
    def __init__(self, vertices, method='difference'):
        self.__vertices = vertices
        self.__vertex_number = len(vertices)
        self.__method = -1
        if method.lower() == 'difference':
            self.__method = 0
        if method.lower() == 'exponential':
            self.__method = 1
        if self.__method == -1:
            raise NotImplementedError('Unknown sampling method: ' + method)


    def sample(self):
        if self.method == 0:
            vertex = self.__sample_difference()
        if self.method == 1:
            vertex = self.__sample_exponential()
        self.__mesh.add_vertex(vertex)

    def __sample_difference(self):
        n_uni = [0.0] + [random.random() for i in xrange(self.__vertex_number - 1)] + [1.0]
        n_uni.sort()
        barycentric = [n_uni[i+1] - n_uni[i] for i in xrange(self.__vertex_number)]
        return self.__barycentric_to_world(barycentric)

    def __sample_exponential(self):
        n_exp = [random.expovariate(1.0) for i in xrange(self.__vertex_number)]
        norm = sum(n_exp)
        barycentric = [exp / norm for exp in n_exp]
        return self.__barycentric_to_world(barycentric)

    def __barycentric_to_world(self, barycentric):
        return reduce(vector_sum, [vector_scale(barycentric[i], self.__vertices[i]) for i in xrange(self.__vertex_number)], [0.0]*self.__vertex_number)
    

class QualityDelaunayCube(AbstractSampler):

    def __init__(self, mesh, bounding_box, quality_parameter=5, quality_threshold=0.1, refinement_strategy=0):
        self.__x0, self.__x1 = bounding_box

        self.__mesh = mesh
        self.__qp = quality_parameter
        self.__qt = quality_threshold
        self.__rs = refinement_strategy
        self.__simplex_quality = SortedDictionary()
        self.__simplex_distance = SortedDictionary()

        simplex_ids = []
        for simplex_id in xrange(self.__mesh.simplex_number()):
            try:
                self.__mesh.node_ids(simplex_id)
            except MeshError:
                continue
            else:
                simplex_ids += [simplex_id]

        self.update_add(simplex_ids)
        
        self.update_sample_box()

    def bad_simplex_number(self):
        return len(self.__simplex_quality)

    def update_sample_box(self):
        if self.__simplex_distance.is_empty():
            return
        max_distance_simplex_id = self.__simplex_distance.max_key()
        self.__delta = self.__simplex_distance.value(max_distance_simplex_id)
        self.__sx0 = vector_sum(self.__x0, [-self.__delta] * self.__mesh.dimension)
        self.__sx1 = vector_sum(self.__x1, [ self.__delta] * self.__mesh.dimension)
        

    def update_remove(self, simplex_ids):
        for simplex_id in simplex_ids:
            try:
                self.__simplex_quality.remove(simplex_id)
            except KeyError:
                pass

            try:
                self.__simplex_distance.remove(simplex_id)
            except KeyError:
                pass

    def update_add(self, simplex_ids):
        for simplex_id in simplex_ids:
            all_nodes_inside = True
            all_nodes_outside = True
            delta = 0.0
            for x in self.__mesh.geometry(simplex_id):
                node_inside = True
                for i in xrange(self.__mesh.dimension):
                    if x[i] < self.__x0[i]:
                        delta = max(delta, self.__x0[i] - x[i])
                        all_nodes_inside = False
                        node_inside = False
                    elif x[i] > self.__x1[i]:
                        delta = max(delta, x[i] - self.__x1[i])
                        all_nodes_inside = False
                        node_inside = False
                if node_inside:
                    all_nodes_outside = False

            if simplex_id == 0:
                self.__simplex_distance.push(simplex_id, delta)
                        
            if not (all_nodes_inside or all_nodes_outside):
#                print "-"*200
#                print simplex_id, self.__mesh.geometry(simplex_id)
                self.__simplex_distance.push(simplex_id, delta)
        
            if not all_nodes_outside:
                quality = self.__mesh.get_simplex_quality(simplex_id)
                qp = quality[self.__qp]
                if qp < self.__qt:
                    self.__simplex_quality.push(simplex_id, qp)
     
    def sample(self):
        is_inserted = False
        while not is_inserted:
            if self.__simplex_quality.is_empty():
                vertex = self.__sample_random()
            else:
                vertex = self.__sample_quality()
            
            try:
                self.__mesh.add_vertex(vertex)
            except ValueError:
                continue
            else:
                is_inserted = True
        
#        print "="*200
#        print "-"*200
#        print self.__mesh.removed_simplex_ids
        self.update_remove(self.__mesh.removed_simplex_ids)
#        print self.__simplex_distance
#        print self.__mesh.added_simplex_ids
        self.update_add(self.__mesh.added_simplex_ids)
#        print self.__simplex_distance
#        print "-"*200
        self.update_sample_box()

    def __sample_random(self):
        return [random.uniform(self.__sx0[i], self.__sx1[i]) for i in xrange(self.__mesh.dimension)]

    def __sample_quality(self):
        simplex_id = self.__simplex_quality.min_key()
        self.__simplex_quality.remove(simplex_id)
        if self.__rs == 0:
            barycentric = self.__mesh.get_simplex_quality(simplex_id)[6]
            geometry = self.__mesh.geometry(simplex_id)
            x = [0.0] * self.__mesh.dimension
            for i in xrange(self.__mesh.dimension+1):
                x = vector_sum(x, vector_scale(barycentric[i], geometry[i]))
        elif self.__rs == 1:
            quality = self.__mesh.get_simplex_quality(simplex_id)
            delta = 0.1 * quality[0] / quality[1]
            longest_edge = quality[4]
            x0 = self.__mesh.get_coordinates(longest_edge[0])
            x1 = self.__mesh.get_coordinates(longest_edge[1])
            x2 = [random.uniform(-delta, delta) for i in xrange(self.__mesh.dimension)]
            x = vector_scale(0.5, vector_sum(vector_sum(x0, x1),x2))
#            x = vector_scale(0.5, vector_sum(x0, x1))
        else:
            x = self.__sample_random()

        return x
