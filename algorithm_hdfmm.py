from utils import PriorityQueue
from algorithm import AlgorithmError
#import itertools

#from convergence_tools import print_cycle_history

inf = float('inf')

class HeuristicDrivenFastMarchingMethod:

    def __init__(self, mesh, results, system, heuristic, boundary, termination, print_flag=1):
        self.pq = PriorityQueue(print_flag)

        for (node_id, V) in boundary.dirichlet():
            H = heuristic(mesh.get_coordinates(node_id))
            self.pq.push([V+H, H, V, []], node_id)

        self.mesh = mesh
        self.results = results
        self.results.node_values_mask = [0, 1, 2]
        self.results.node_values_names = ['Key', 'H', 'V', 'char_nodes', 'char_weights']
        self.system = system
        self.heuristic = heuristic
        self.boundary = boundary
        self.termination = termination


    def step(self):
        if self.pq.is_empty():
            raise AlgorithmError('Environment is disconnected, or the termination criteria is incorrect')

        V, node_id = self.pq.pop()
#        print "V =", V
#        print "node_id =", node_id

        try:
            V_previous = self.results.getnode(node_id)
#            print "V_node =", V_node
        except KeyError:
            pass
        else:
            if V_previous[0] < V[0]:
#                print "Early return"
                return V, node_id

        self.results.addnode(node_id, V)

        for simplex_id in self.mesh.star(node_id):
            
            for node_id_neighbor in self.mesh.node_ids(simplex_id):
                if node_id_neighbor == node_id:
                    continue

                node_ids = self.mesh.node_ids_local(simplex_id, node_id_neighbor)

                nodes = []; V = []
                for node_id_neighbor_2 in node_ids:
                    nodes += [self.mesh.get_coordinates(node_id_neighbor_2)]
                    try:
                        V_node = self.results.getnode(node_id_neighbor_2)[2]
                    except KeyError:
                        V_node = float('inf')
                    V += [V_node]
                    

                V_neighbor, j_dep, beta = self.system.update(nodes, V)
                node_ids_dep = [nodes[j] for j in j_dep]
                
                H_neighbor = self.heuristic(self.mesh.get_coordinates(node_id_neighbor))

                try:
                    V_previous = self.results.getnode(node_id_neighbor)
                except KeyError:
                    self.pq.push([V_neighbor+H_neighbor, H_neighbor, V_neighbor, node_ids_dep, beta], node_id_neighbor)
                else:
                    if V_neighbor+H_neighbor < V_previous[0]:
                        self.pq.push([V_neighbor+H_neighbor, H_neighbor, V_neighbor, node_ids_dep, beta], node_id_neighbor)

                

        return V, node_id

    def plan(self):
        while not self.termination.terminate():
            V, node_id = self.step()
            self.termination.update_point(V, node_id)
            

