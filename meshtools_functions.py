from math import sin, floor
try:
    import Image
except ImportError:
    from PIL import Image

class Sinusoid:
    def __init__(self, dimension, amplitude, center_amplitude, frequency, phase):
        self.dimension = dimension
        self.amplitude = amplitude
        self.center_amplitude = center_amplitude
        self.frequency = frequency
        self.phase = phase

    def value(self, x):
        result = 0.0
        for i in xrange(self.dimension):
            result += self.amplitude[i] * sin(self.frequency[i] * x[i] + self.phase[i]) + \
                self.center_amplitude[i]

        return result

class WrongSinusoid:
    def __init__(self, dimension, amplitude, center_amplitude, frequency):
        self.dimension = dimension
        self.amplitude = amplitude
        self.center_amplitude = center_amplitude
        self.frequency = frequency

    def value(self, x):
        result = self.amplitude
        for i in xrange(self.dimension):
            result *=  sin(self.frequency[i] * x[i])

        result += self.center_amplitude

        return result

class ImageLinearSat:
    def __init__(self, image_filename, coordinates, low, high, sat_low=None, sat_high=None, interpolation=0):
        if sat_low == None:
            sat_low = low
        if sat_high == None:
            sat_high = high

        bitmap = Image.open(image_filename).convert("L")
        self.bitmap_x, self.bitmap_y = bitmap.size
        self.bitmap_data = bitmap.getdata()
        self.x0, self.y0, self.x1, self.y1 = coordinates
        self.hx = (self.x1 - self.x0) / (self.bitmap_x - 1)
        self.hy = (self.y1 - self.y0) / (self.bitmap_y - 1)
        self.low = low
        self.high = high
        self.sat_low = sat_low
        self.sat_high = sat_high
        self.interpolation = interpolation

    def ixy(self, ix, iy):
        return ix + self.bitmap_x * (self.bitmap_y - 1 - iy)
    
    def val(self, pix):
        if pix == 0:
            return self.sat_low
        elif pix == 255:
            return self.sat_high
        else:
            return self.low + (self.high - self.low) * pix / 255

    def value(self, coord):
        x = coord[0]; y = coord[1]
        if self.interpolation == 0:
            ix0 = int((x - self.x0) / self.hx)
            iy0 = int((y - self.y0) / self.hy)
            if ix0 < 0:
                ix0 = 0
            if iy0 < 0:
                iy0 = 0
            if ix0 >= self.bitmap_x:
                ix0 = self.bitmap_x - 1
            if iy0 >= self.bitmap_y:
                iy0 = self.bitmap_y - 1

            return self.val(self.bitmap_data[self.ixy(ix0, iy0)])

        elif self.coordinate_interpolation == 1:
            ix0 = floor((x - self.x0) / self.hx)
            iy0 = floor((y - self.y0) / self.hy)
            ix1 = (ix0 + 1)
            iy1 = (iy0 + 1)
            alpha_x = x / self.hx - ix0
            alpha_y = y / self.hx - ix0

            if ix0 < 0:
                ix0 = 0; ix1 = 0
            if iy0 < 0:
                iy0 = 0; iy1 = 0

            if ix0 >= self.bitmap_x:
                ix0 = self.bitmap_x - 1; ix1 = ix0
            if iy0 >= self.bitmap_y:
                iy0 = self.bitmap_y - 1; iy1 = iy0

            vx0y0 = self.val(self.bitmap_data[self.ixy(ix0, iy0)])
            vx1y0 = self.val(self.bitmap_data[self.ixy(ix1, iy0)])
            vx0y1 = self.val(self.bitmap_data[self.ixy(ix0, iy1)])
            vx1y1 = self.val(self.bitmap_data[self.ixy(ix1, iy1)])

            return (vx0y0 * (1.0 - alpha_x) + vx1y0 * alpha_x) * (1.0 - alpha_y) + (vx0y1 * (1.0 - alpha_x) + vx1y1 * alpha_x) * alpha_y
        else:
            return 1.0

