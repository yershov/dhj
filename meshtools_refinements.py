import utils
import itertools

class BisectionRefinement:
    
    def __init__(self, mesh, mesh_marker):
        self.mesh = mesh
        self.mesh_marker = mesh_marker

        if self.mesh.cartesian_number > 1:
            raise RefinementError('Bisection Refinement supports simplicial meshes only')

#        self.reinitialize()

    def reinitialize(self):
        self.mesh_marker.reinitialize()
        
    def refine(self):
        simplex_ids = self.mesh_marker.mark_simplices()[0]

        refine_edges_sorted = utils.SortedList()

        while simplex_ids:
            simplex_id = simplex_ids.pop(0)

            h_min, aspect, _, _, node_max = self.mesh.quality[0][simplex_id]
            h_max = aspect / h_min

            try:
                refine_edges_sorted.search((h_max, node_max))
            except ValueError:
                refine_edges_sorted.insert((h_max, node_max))
                
                simplex_ids_neighbor = list(set.intersection(set(self.mesh.neighbors[0][node_max[0]]), 
                                                             set(self.mesh.neighbors[0][node_max[1]])))

                simplex_ids += simplex_ids_neighbor

        refine_edges = []
                    
        while True:
            try:
                _,node_ids = refine_edges_sorted.pop(-1)
            except IndexError:
                break

            refine_edges += [node_ids + (0.5,)]

        refine_edges = [refine_edges]

        self.mesh.refine_edges(refine_edges)
        self.mesh.precompute_barycentric()
        self.mesh.precompute_quality()
        self.reinitialize()


class CharacteristicsRefinement:
    
    def __init__(self, mesh, results, mesh_marker, badness=0.95):
        self.mesh = mesh
        self.results = results
        
        self.mesh_marker = mesh_marker

        self.badness = badness

        if self.mesh.cartesian_number > 1:
            raise RefinementError('Characteristics Refinement supports simplicial meshes only')

#        self.reinitialize()

    def reinitialize(self):
        self.mesh_marker.reinitialize()

        if self.results.node_values_names == None:
            return

    def refine(self):
        self.i_index = self.results.node_values_names.index('char_nodes')
        self.w_index = self.results.node_values_names.index('char_weights')

        node_ids = self.mesh_marker.mark_nodes()[0]

        refine_edges = []

        while node_ids:
            node_id = node_ids.pop(0)

            values = self.results.getnode((node_id, ))

            node_ids_neighbors = values[self.i_index]
            node_weights = values[self.w_index]
            
            if len(node_ids_neighbors) == 0:
                print "These nodes should no be marked"
                continue
            elif len(node_ids_neighbors) == 1:
                continue

            neighbors = list(itertools.izip(node_weights, node_ids_neighbors))
            neighbors.sort(reverse=True)
            neighbor1 = neighbors[0]
            neighbor2 = neighbors[1]
            node_id_neighbor1 = neighbor1[1]
            node_id_neighbor2 = neighbor2[1]

            alpha = neighbor1[0] / (neighbor1[0] + neighbor2[0])
            if alpha > self.badness:
                alpha = self.badness
            elif alpha < (1.0 - self.badness):
                alpha = 1.0 - self.badness
#            if alpha >= (1.0 - self.badness) and alpha <= self.badness:
            refine_edges += [(node_id_neighbor1[0], node_id_neighbor2[0], alpha)]
#            refine_edges += [(node_id_neighbor1[0], node_id[0], alpha)]
#            refine_edges += [(node_id[0], node_id_neighbor2[0], alpha)]                    

        refine_edges = [refine_edges]

        self.mesh.refine_edges(refine_edges)
        self.mesh.precompute_barycentric()
        self.mesh.precompute_quality()
        self.reinitialize()
