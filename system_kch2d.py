from math import sin, cos
from system import SystemError
from system_omni import *
from utils import *

eps = 1.e-8
inf = float('inf')

def line_intersect(x1, x2, x3, x4):
    x11, y11 = x1
    x12, y12 = x2
    x21, y21 = x3
    x22, y22 = x4
#  Solve system:
#  [ x11 ]          [ x12 ]                [ x21 ]          [ x22 ]
#  [     ] alpha1 + [     ] (1 - alpha1) = [     ] alpha2 + [     ] (1 - alpha2)
#  [ y11 ]          [ y12 ]                [ y21 ]          [ y22 ]
#
# or similarly
#
#  [ x11 - x12   x22 - x21 ]  [ alpha1 ]   [ x22 - x12 ]
#  [                       ]  [        ] = [           ] 
#  [ y11 - y12   y22 - y21 ]  [ alpha2 ]   [ y22 - y12 ]

#    print x11, y11
#    print x12, y12
#    print x21, y21
#    print x22, y22

    a11 = x11 - x12; a12 = x22 - x21
    a21 = y11 - y12; a22 = y22 - y21
    b1 = x22 - x12; b2 = y22 - y12

    det = a11 * a22 - a21 * a12
    
    if abs(det) < eps:
        return [inf, inf]
    
    alpha1 = (  a22 * b1 - a12 * b2) / det
    alpha2 = (- a21 * b1 + a11 * b2) / det

    return [alpha1, alpha2]


class KinematicChain2D(OmniVehicle):

    def __init__(self, mesh, chain, obstacles, self_collision=False):
        super(KinematicChain2D, self).__init__(mesh)
        self.chain = chain
        self.obstacles = obstacles
        self.self_collision = self_collision
        

    def update(self, nodes, V):
        x = nodes[0]

        if self.collision_check(x):
            return inf, [], []

        return super(KinematicChain2D, self).update(nodes, V)


    def update_omni(self, nodes, V):
        return super(KinematicChain2D, self).update(nodes, V)


    def arm_configuration(self, x):
        x0 = [0.0, 0.0]
        theta0 = 0.0
        
        arm_conf = []

        for link in self.chain:
            i, l, tmin, tmax, s0, t0, link_type = link
            theta = t0 + theta0
            s = s0
            if link_type != 0:
                t = tmin + (tmax - tmin) * x[i]
            if link_type == 1:
                theta = t + theta0
            if link_type == 2:
                s = t

            vector_l = [l * cos(theta), l * sin(theta)]
            x1 = vector_sum(x0, vector_l)
            arm_conf.append([x0, x1])
            x0 = vector_sum(x0, vector_scale(s, vector_l))
            theta0 = theta
            
        return arm_conf


    def collision_check(self, x):
        arm_conf = self.arm_configuration(x)
        for i in xrange(len(arm_conf)):
            x1, x2 = arm_conf[i]
            for x3, x4 in self.obstacles:
                alpha1, alpha2 = line_intersect(x1, x2, x3, x4)
                if alpha1 >= 0.0 and alpha1 <= 1.0 and alpha2 >= 0.0 and alpha2 <= 1.0:
                    return True

        if self.self_collision:
            for i in xrange(len(arm_conf)):
                x1, x2 = arm_conf[i]
                for j in xrange(i - 1):
                    x3, x4 = arm_conf[j]                
                    alpha1, alpha2 = line_intersect(x1, x2, x3, x4)
                    if alpha1 >= 0.0 and alpha1 <= 1.0 and alpha2 >= 0.0 and alpha2 <= 1.0:
                        return True
                
        return False
