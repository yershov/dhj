from utils import PriorityQueue, SortedList
from algorithm import AlgorithmError

inf = float('inf')
V_ID = 0

class FastMarchingMethod:

    def __init__(self, mesh, results, system, boundary, termination, print_flag=1):
        self.mesh = mesh
        self.results = results
        self.results.node_values_mask = [0]
        self.results.node_values_names = ['V', 'char_nodes', 'char_weights', 'char_simplex']
        self.system = system
        self.boundary = boundary
        self.termination = termination
        self.termination_list = SortedList(self.termination.mark_nodes())
        self.print_flag = print_flag

        self.pq = PriorityQueue(self.print_flag)

        for (node_id, V) in self.boundary.dirichlet():
            self.results.addnode(node_id, [V, [], [], None])
            self.pq.push(V, node_id)
        
        self.fmm_iteration = 0
        self.n_update = 0


    def step(self):
        V, node_id = self.pq.pop()

        for simplex_id in self.mesh.star(node_id):
            for node_id_neighbor in self.mesh.node_ids(simplex_id):
                if node_id_neighbor == node_id:
                    continue

                if self.boundary.isboundary(node_id_neighbor):
                    continue

                try:
                    values = self.results.getnode(node_id_neighbor)
                except KeyError:
                    V_neighbor = float('inf')
                else:
                    V_neighbor = values[V_ID]

                if V_neighbor >= V:

                    node_ids = list(self.mesh.node_ids_local(simplex_id, node_id_neighbor))

                    nodes = []; V_nodes = []
                    for node_id_neighbor_2 in node_ids:
                        nodes.append(self.mesh.get_coordinates(node_id_neighbor_2))
                        try:
                            V_node = self.results.getnode(node_id_neighbor_2)[0]
                        except KeyError:
                            V_node = float('inf')
                        V_nodes.append(V_node)

                    V_update, j_dep, beta = self.system.update(nodes, V_nodes)
                    self.n_update += 1
                
                    if V_neighbor > V_update:
                        node_ids_dep = [node_ids[j] for j in j_dep]
                        
                        self.results.addnode(node_id_neighbor, [V_update, node_ids_dep, beta, simplex_id])
                        self.pq.push(V_update, node_id_neighbor)
                        
                        if self.mesh.has_periodic_boundary and self.mesh.is_periodic(node_id_neighbor):
                            for node_id_neighbor_periodic in self.mesh.periodic_connections(node_id_neighbor):
                                self.results.addnode(node_id_neighbor_periodic, [V_update, node_ids_dep, beta, simplex_id])
                                self.pq.push(V_update, node_id_neighbor_periodic)
                                

        return V, node_id

    def plan(self):
        is_term = False
        while not is_term:
            if self.pq.is_empty():
                raise AlgorithmError('Environment is disconnected')
            
            self.fmm_iteration += 1

            V, node_id = self.step()

            if self.termination.test_node(node_id):
                is_term = True
                for node_id in self.termination_list:
                    try:
                        values  = self.results.getnode(node_id)
                    except KeyError:
                        is_term = False


# redesign replanning strategy, and make it more like lifelong 
    # def replan(self, node_ids):
    #     self.reinitialize()
        
    #     for node_id in node_ids:
    #         V_min = float('inf')
    #         node_ids_dep_min = []
    #         beta_min = []
    #         simplex_id_min = None
    #         for simplex_id in self.mesh.star(node_id):
    #             nodes = []
    #             V = []
    #             node_ids = self.mesh.node_ids_local(simplex_id, node_id)
    #             for node_id_neighbor in node_ids:
    #                 nodes += [self.mesh.get_coordinates(node_id_neighbor)]
    #                 try:
    #                     V_node = self.results.getnode(node_id_neighbor)[0]
    #                 except KeyError:
    #                     V_node = float('inf')
    #                 V += [V_node]

    #             V_neighbor, j_dep, beta = self.system.update(nodes, V)
                
    #             if V_neighbor < V_min:
    #                 V_min = V_neighbor
    #                 node_ids_dep_min = [node_ids[j] for j in j_dep]
    #                 beta_min = beta
    #                 simplex_id_min = simplex_id

    #         self.results.addnode(node_id, [V_min, node_ids_dep_min, beta_min, simplex_id_min])
    #         self.pq.push([V_min, node_ids_dep_min, beta_min, simplex_id_min], node_id)

    #     self.plan()
