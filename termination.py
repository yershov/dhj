import bisect
import math
import numpy as np

class TerminationError(Exception):
    def __init__(self, value=""):
        self.value = value
    def __str__(self):
        return repr(self.value)


class TerminationPoint:

    def __init__(self, mesh, x):
        self.__mesh = mesh
        self.__x = x
        self.__simplex_id = None
        self.reinitialize()
        
    def reinitialize(self):
        self.__simplex_id = self.__mesh.find_simplex(self.__x, self.__simplex_id)
        self.__node_ids = list(self.__mesh.node_ids(self.__simplex_id))
        self.__node_ids.sort()

    def update_point(self, V, node_id):
        try:
            self.__node_ids.remove(node_id)
        except ValueError:
            pass

    def update_region(self, *dummy):
        pass
        
    def terminate(self):
        return False if self.__node_ids else True

    def termination_simplex(self):
        return self.__simplex_id

    def termination_nodes(self):
        return self.__node_ids

    def istermination_node(self, node_id):
        index = bisect.bisect_left(self.__node_ids, node_id)
        return (index < len(self.__node_ids) and self.__node_ids[index] == node_id)


class TerminationIteration:

    def __init__(self, iteration_max):
        self.__iteration_max = iteration_max
        self.reinitialize()
        
    def reinitialize(self):
        self.__iteration = 0

    def update_iteration(self):
        self.__iteration += 1
        
    def terminate(self):
        return self.__iteration >= self.__iteration_max

