import utils
import itertools

class CharacteristicsRefinement:
    
    def __init__(self, x, h_min, aspect_min, mesh, results):
        self.x = x
        self.h_min = h_min
        self.aspect_min2 = aspect_min**2
        self.mesh = mesh
        self.results = results
        self.simplex_id = None

        if self.mesh.cartesian_number > 1:
            raise RefinementError('Heuristic Refinement supports simplicial meshes only')

        self.reinitialize()

    def reinitialize(self):
        self.simplex_id = self.mesh.find_simplex(self.x, self.simplex_id)
        self.ini_node_ids = self.mesh.node_ids(self.simplex_id)

        
    def refine(self):
        v_index = self.results.node_values_names.index('V')
        i_index = self.results.node_values_names.index('char_nodes')
        w_index = self.results.node_values_names.index('char_weights')

        node_ids = list(self.ini_node_ids)

        refine_edges = []
        
        while node_ids:
            node_id = node_ids.pop(0)
            
            try:
                values = self.results.getnode(node_id)
            except KeyError:
                continue

            node_ids_neighbors = values[i_index]
            node_weights = values[w_index]

            if len(node_ids_neighbors) == 0:
#
#   Check if the goal is a point
#
                for simplex in self.mesh.star(node_id):
                    is_boundary_simplex = True
                    for node_id_neighbor in self.mesh.node_ids(simplex):
                        try:
                            values = self.results.getnode(node_id_neighbor)
                        except KeyError:
                            is_boundary_simplex = False
                            break
                        else:
                            if values[v_index] != 0.0:
                                is_boundary_simplex = False
                                break

                    if is_boundary_simplex:
                        node_id_max_1 = None
                        node_id_max_2 = None
                        dist_max = 0.0
                        for node_id_neighbor_1 in self.mesh.node_ids(simplex):
                            for node_id_neighbor_2 in self.mesh.node_ids(simplex):
                                x1 = self.mesh.get_coordinates(node_id_neighbor_1)
                                x2 = self.mesh.get_coordinates(node_id_neighbor_2)

                                dist = sum(map(lambda a, b: (a - b)**2, x1, x2))
                                if dist > dist_max:
                                    node_id_max_1 = node_id_neighbor_1
                                    node_id_max_2 = node_id_neighbor_2
                                    dist_max = dist
                                    
                        
                        if dist_max > self.h_min**2:
                            refine_edges += [(node_id_max_1[0], node_id_max_2[0], 0.5)]

                        break

                continue

            node_ids += node_ids_neighbors

            is_near_vertex = False
            
            for ind in xrange(len(node_weights)):
                beta = node_weights[ind]
                if beta > 0.90:
                    is_near_vertex = True

                    # node_id_neighbor = node_ids_neighbors[ind]
                    # x1 = self.mesh.get_coordinates(node_id)
                    # x2 = self.mesh.get_coordinates(node_id_neighbor)

                    # dist = sum(map(lambda a, b: (a - b)**2, x1, x2))

                    # if dist > self.h_min**2:
                    #     refine_edges += [(node_id[0], node_id_neighbor[0], 0.5)]
                        
                    
            if is_near_vertex:
                continue

            neighbors = list(itertools.izip(node_weights, node_ids_neighbors))
            neighbors.sort(reverse=True)
            neighbor1 = neighbors[0]
            neighbor2 = neighbors[1]
            node_id_neighbor1 = neighbor1[1]
            node_id_neighbor2 = neighbor2[1]

            x0 = self.mesh.get_coordinates(node_id)
            x1 = self.mesh.get_coordinates(neighbor1[1])
            x2 = self.mesh.get_coordinates(neighbor2[1])

            dist01 = sum(map(lambda a, b: (a - b)**2, x0, x1))
            dist02 = sum(map(lambda a, b: (a - b)**2, x0, x2))
            dist12 = sum(map(lambda a, b: (a - b)**2, x1, x2))


            if dist12 < self.aspect_min2 * dist01 or dist12 < self.aspect_min2 * dist02:
                if dist01 > dist02:
                    refine_edges += [(node_id[0], node_id_neighbor1[0], 0.5)]
                else:
                    refine_edges += [(node_id[0], node_id_neighbor2[0], 0.5)]                    
            else:
                alpha = neighbor1[0] / (neighbor1[0] + neighbor2[0])
                refine_edges += [(node_id_neighbor1[0], node_id_neighbor2[0], alpha)]
                refine_edges += [(node_id_neighbor1[0], node_id[0], alpha)]
                refine_edges += [(node_id[0], node_id_neighbor2[0], alpha)]                    
                    
            
        refine_edges = [refine_edges]

        self.mesh.refine_edges(refine_edges)
        self.mesh.precompute_barycentric()
